package org.bitbucket.httpserver.tp;

import org.bitbucket.customHttpServer.SimpleHttpRequest;
import org.bitbucket.customHttpServer.SimpleHttpResponse;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Handler implements Runnable{

    private int port;

    private ServerSocket serverSocket;

    protected Thread runningThread = null;

    private final ExecutorService threadPool = Executors.newFixedThreadPool(10);

    public Handler(int port) {
        this.port = port;
    }

    public Handler() {

    }

    @Override
    public void run() {
        String runningThreadName;
        synchronized(this){
            this.runningThread = Thread.currentThread();
            runningThreadName = this.runningThread.getName();
        }
        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            try {
                throw new IOException("Failed to open port " + port);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        int countThread = 0;
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                SimpleHttpRequest req = new SimpleHttpRequest();
                SimpleHttpResponse resp = new SimpleHttpResponse();
                req.takeRequest(socket);
                this.threadPool.execute(new Handler());
                this.executeMethod(req, resp);
                resp.sendResponse(socket);
                this.runningThread = new Thread(runningThreadName + " " + countThread++);
                System.out.println(this.runningThread.getName());

            } catch (IOException ignore) {
            }
        }
    }

    public void executeMethod(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        switch (req.getMethod()) {
            case "GET":
                this.doGet(req, resp);
                break;
            case "HEAD":
                this.doHead(req, resp);
                break;
            case "POST":
                this.doPost(req, resp);
                break;
            case "PUT":
                this.doPut(req, resp);
                break;
            case "DELETE":
                this.doDelete(req, resp);
                break;
            case "CONNECT":
                this.doConnect(req, resp);
                break;
            case "OPTIONS":
                this.doOptions(req, resp);
                break;
            case "TRACE":
                this.doTrace(req, resp);
                break;
            case "PATCH":
                this.doPatch(req, resp);
                break;
            default:
                throw new IOException("Method " + req.getMethod() + " is allowed.");
        }
    }

    public void doGet(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doGet</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doPost(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPost</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doHead(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doHead</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
    }

    public void doPut(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPut</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doDelete(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doDelete</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doConnect(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doConnect</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doOptions(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doOptions</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doTrace(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doTrace</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doPatch(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPatch</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }
}
