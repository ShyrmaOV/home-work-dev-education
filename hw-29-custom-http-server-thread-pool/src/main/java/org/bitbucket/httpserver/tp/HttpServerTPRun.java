package org.bitbucket.httpserver.tp;

public class HttpServerTPRun {

    public static void main(String[] args) {
        Handler socketHandler = new Handler(8040);
        Thread socketThread = new Thread(socketHandler, "Server");
        socketThread.start();
        System.out.println(socketThread.getName() + " run.");
    }
}
