package org.bitbucket.log;

import org.apache.log4j.Logger;

public class RandomNumber {

    private int randomNumber;

    private static final Logger log = Logger.getLogger(RandomNumber.class);

    public int getRandomNumber() {
        try {
            randomNumber = (int) (Math.random() * 10);
            if (randomNumber >= 5 ) {
                log.info("Приложение успешно запущено");
                return randomNumber;
            }
            throw new LogException("Сгенерированное число - " + randomNumber);
        } catch (LogException le) {
            log.error(le.getMessage());
            return randomNumber;
        }
    }
}
