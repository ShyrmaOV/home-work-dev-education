package org.bitbucket.log;

public class LogException extends RuntimeException {

    public LogException(String message) {
        super(message);
    }
}
