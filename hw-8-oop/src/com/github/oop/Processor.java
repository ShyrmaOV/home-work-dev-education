package com.github.oop;

public abstract class Processor {
    double frequency;
    double cache;
    double binCapacity;

    public Processor(double frequency, double cache, double binCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.binCapacity = binCapacity;
    }

    public String getDetails() {
        return frequency + " " + cache + " " + binCapacity;
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);
}
