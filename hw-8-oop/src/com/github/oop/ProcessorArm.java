package com.github.oop;

public class ProcessorArm extends Processor {
    private final String architecture = "ARM";

    public ProcessorArm(double frequency, double cache, double binCapacity) {
        super(frequency, cache, binCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if (data == null ) {
            return null;
        }
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return "" + data;
    }

    public String getArchitecture() {
        return architecture;
    }
}
