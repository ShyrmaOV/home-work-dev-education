package com.github.oop;

public class MemoryInfo {

    private int memorySize;

    private double memorySizePercent;

    public MemoryInfo(int memorySize, double memorySizePercent) {
        this.memorySize = memorySize;
        this.memorySizePercent = memorySizePercent;
    }

    @Override
    public String toString() {
        return "memorySize = " + memorySize +
                ", memorySizePercent = " + memorySizePercent + "%";
    }
}
