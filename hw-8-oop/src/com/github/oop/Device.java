package com.github.oop;

public class Device {
    Processor processor;
    Memory memory;

    public Device(Processor processor, Memory memory){
        this.processor = processor;
        this.memory = memory;
    }

    void save(String[] data) {
    }

    public String[] readAll() {
        return memory.memoryCell;
    }

    void dataProcessing() {

    }

    public String getSystemInfo() {
        return processor.getDetails() + " " + memory.getMemoryInfo();
    }

}
