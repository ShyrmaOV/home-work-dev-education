package com.github.oop.test;

import com.github.oop.ProcessorX86;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProcessorX86Test {

    ProcessorX86 processorX86 = new ProcessorX86(1.5, 2, 4);

    @Test
    public void dataProcessString() {
        String exp = "call";
        String act = processorX86.dataProcess("call");
        assertEquals(exp, act);
    }

    @Test
    public void dataProcessStringNull() {
        String act = processorX86.dataProcess(null);
        assertNull(act);
    }

    @Test
    public void dataProcessLong() {
        String exp = "5";
        String act = processorX86.dataProcess(5);
        assertEquals(exp, act);
    }

    @Test
    public void getArchitecture() {
        String exp = "X86";
        String act = processorX86.getArchitecture();
        assertEquals(exp, act);
    }
}