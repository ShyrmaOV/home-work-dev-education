package com.github.oop.test;

import com.github.oop.Device;
import com.github.oop.Memory;
import com.github.oop.Processor;
import com.github.oop.ProcessorArm;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeviceTest {

    Memory memory = new Memory();
    Device device = new Device(new ProcessorArm(1,1,1) {
    }, new Memory(new String[5]));
    @Test
    public void save() {
    }

    @Test
    public void readAll() {
        String[] exp = {null, null, "f", null, "t"};
        String[] act = device.readAll();
        assertEquals(exp, act);
    }

    @Test
    public void dataProcessing() {
    }

    @Test
    public void getSystemInfo() {
        String exp = "1.0 1.0 1.0 memorySize = 5, memorySizePercent = 0.0%";
        String act = device.getSystemInfo();
        assertEquals(exp, act);
    }
}