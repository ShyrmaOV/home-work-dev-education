package com.github.oop.test;

import com.github.oop.ProcessorArm;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProcessorArmTest {

    ProcessorArm processorArm = new ProcessorArm(1.5, 2, 4);

    @Test
    public void dataProcessString() {
        String exp = "CALL";
        String act = processorArm.dataProcess("call");
        assertEquals(exp, act);
    }

    @Test
    public void dataProcessStringNull() {
        String act = processorArm.dataProcess(null);
        assertNull(act);
    }

    @Test
    public void dataProcessLong() {
        String exp = "5";
        String act = processorArm.dataProcess(5);
        assertEquals(exp, act);
    }

    @Test
    public void getArchitecture() {
        String exp = "ARM";
        String act = processorArm.getArchitecture();
        assertEquals(exp, act);
    }
}