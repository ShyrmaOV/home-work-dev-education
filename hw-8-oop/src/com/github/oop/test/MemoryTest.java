package com.github.oop.test;

import com.github.oop.Memory;
import com.github.oop.MemoryInfo;
import org.junit.Test;

import static org.junit.Assert.*;

public class MemoryTest {

    String[] globalData = new String[] {"a", "b", "c"};

    @Test
    public void readLast() {
        String exp = "c";
        String act = new Memory(globalData).readLast();
        assertEquals(exp, act);
    }

    @Test
    public void removeLast() {
        String act = new Memory(globalData).removeLast();
        assertNull(act);
    }

    @Test
    public void save() {
        String[] data = new String[] {"a", "b", null};
        boolean act = new Memory(data).save("d");
        assertTrue(act);
    }

    @Test
    public void getMemoryInfo() {
        MemoryInfo exp = new MemoryInfo(3, 0.06);
        String[] data = new String[] {"null", "b", null};
        Object act = new Memory(data).getMemoryInfo();
        assertEquals(exp, act);
    }
}