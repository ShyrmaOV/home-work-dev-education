package com.github.oop;

public class ProcessorX86 extends Processor {
    private final String architecture = "X86";

    public ProcessorX86(double frequency, double cache, double binCapacity) {
        super(frequency, cache, binCapacity);
    }

    @Override
    public String dataProcess(String data) {
        if (data == null ) {
            return null;
        }
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return "" + data;
    }

    public String getArchitecture() {
        return architecture;
    }
}
