package com.github.oop;

public class Memory {

    String[] memoryCell;

    public Memory(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public String readLast() {
        return memoryCell[memoryCell.length - 1];
    }

    public String removeLast() {
        return memoryCell[memoryCell.length - 1] = null;
    }
    public boolean save(String element) {
        if (memoryCell[memoryCell.length - 1] == null) {
            memoryCell[memoryCell.length - 1] = element;
            return true;
        }
        else {
            return false;
        }
    }
    public MemoryInfo getMemoryInfo() {
        int count = 0;
        double countPercent = 0.0D;
        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] != null) {
                count++;
            }
        }
        countPercent = (double)memoryCell.length / 100 * count;
        return new MemoryInfo(memoryCell.length, countPercent);
    }
}
