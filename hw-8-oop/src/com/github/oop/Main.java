package com.github.oop;


public class Main {
    public static void main(String[] args) {
        ProcessorArm processorArm_1 = new ProcessorArm(1.5, 2, 4);
        ProcessorArm processorArm_2 = new ProcessorArm(2.5, 2, 8);
        ProcessorArm processorArm_3 = new ProcessorArm(3, 3, 6);
        ProcessorX86 processorX86_1 = new ProcessorX86(2.5, 3, 1.5);
        ProcessorX86 processorX86_2 = new ProcessorX86(2.5, 3.5, 2);
        ProcessorX86 processorX86_3 = new ProcessorX86(4,3, 3.5);
        Memory memory_1 = new Memory(new String[6]);
        Memory memory_2 = new Memory(new String[10]);
        Memory memory_3 = new Memory(new String[15]);
//        System.out.println(processorArm_1.);
        System.out.println(memory_1.getMemoryInfo().toString());
        System.out.println(memory_2.getMemoryInfo().toString());
        System.out.println(memory_3.getMemoryInfo().toString());

        Device[] device = new Device[10];
        device[0] = new Device(processorArm_1, memory_1);
        device[1] = new Device(processorArm_2, memory_2);
        device[2] = new Device(processorArm_3, memory_3);
        device[3] = new Device(processorX86_1, memory_1);
        device[4] = new Device(processorX86_2, memory_2);
        device[5] = new Device(processorX86_3, memory_3);
        device[6] = new Device(processorArm_1, memory_2);
        device[7] = new Device(processorArm_3, memory_1);
        device[8] = new Device(processorX86_1, memory_3);
        device[9] = new Device(processorX86_3, memory_1);

//        System.out.println(processorArm.getArchitecture());
//        System.out.println(processorArm.dataProcess(23));
//        System.out.println(processorArm.dataProcess("arm"));
//
//        System.out.println(processorX86.getArchitecture());
//        System.out.println(processorX86.dataProcess(23));
//        System.out.println(processorX86.dataProcess("arm"));
//
//        System.out.println(device.getSystemInfo());

//        memory.memoryCell
    }
}
