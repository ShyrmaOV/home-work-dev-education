package org.bitbucket.webSocket;

public class CustomWebSocket {

    public static Thread thread;

    public static void main(String[] args) {
        CustomWebsocketServer websocketServer = new CustomWebsocketServer();
        thread = new Thread(websocketServer, "WebSocket Thread");
        System.out.println(thread.getName() + " is run");
        thread.start();
    }
}
