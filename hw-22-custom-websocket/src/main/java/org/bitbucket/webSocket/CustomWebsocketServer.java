package org.bitbucket.webSocket;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomWebsocketServer implements Runnable {

    private final ExecutorService threadPool = Executors.newFixedThreadPool(9);

    private boolean isRun = false;

    public void run() {
        isRun = true;
        try (ServerSocket server = new ServerSocket(80)) {
            System.out.println("Server has started on 127.0.0.1:80.\r\nWaiting for a connection...");
            Socket client = server.accept();
            System.out.println("A client connected.");
            InputStream in = client.getInputStream();
            OutputStream out = client.getOutputStream();
            Scanner s = new Scanner(in, StandardCharsets.UTF_8);
            while (isRun) {
                while (in.available() < 3) ;
                    byte[] bytes = new byte[in.available()];
                    in.read(bytes, 0, in.available());
                    String data = new String(bytes, StandardCharsets.UTF_8);
                    Matcher get = Pattern.compile("^GET").matcher(data);
                    threadPool.execute(() -> {
                        try {
                            if (get.find()) {
                                Matcher match = Pattern.compile("Sec-WebSocket-Key: (.*)").matcher(data);
                                match.find();
                                byte[] response = ("HTTP/1.1 101 Switching Protocols\r\n"
                                        + "Connection: Upgrade\r\n"
                                        + "Upgrade: websocket\r\n"
                                        + "Sec-WebSocket-Accept: "
                                        + Base64.getEncoder().encodeToString(MessageDigest.getInstance("SHA-1").digest((match.group(1) + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes("UTF-8")))
                                        + "\r\n\r\n").getBytes("UTF-8");
                                out.write(response, 0, response.length);
                            } else {
                                boolean mask = (bytes[1] + 0b10000000) != 0;
                                int msglen = bytes[1] + 128,
                                        offset = 2;
                                if (msglen == 126) {
                                    msglen = BitConverter.toInt16(new byte[]{bytes[3], bytes[2]}, 0);
                                    offset = 4;
                                    System.out.println("msglen == 126");

                                } else if (msglen == 127) {
                                    System.out.println("msglen == 127");
                                }
                                if (msglen == 0) {
                                    isRun = false;
                                    this.threadPool.shutdown();
                                    System.out.println("Server close!");
                                    return;
                                } else if (mask) {
                                    byte[] decoded = new byte[msglen];
                                    byte[] masks = new byte[]{bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]};
                                    offset += 4;
                                    for (int i = 0; i < msglen; ++i)
                                        decoded[i] = (byte) (bytes[offset + i] ^ masks[i % 4]);
                                    String text = new String(decoded, StandardCharsets.UTF_8);
                                    System.out.println(
                                            Thread.currentThread().getName() + " send text message: " + text
                                    );
                                } else {
                                    System.out.println("mask bit not set");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
