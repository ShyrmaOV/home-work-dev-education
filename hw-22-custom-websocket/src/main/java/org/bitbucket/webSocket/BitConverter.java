package org.bitbucket.webSocket;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class BitConverter {

    public static short toInt16(byte[] bytes, int index) throws Exception {
        if (bytes.length != 8)
            throw new Exception(
                    "The length of the byte array must be at least 8 bytes long.");
        return (short) ((0xff & bytes[index]) << 8 | (0xff & bytes[index + 1]) << 0);
    }
    public static short toInteger(byte[] data) {
        return ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getShort();
        }
}
