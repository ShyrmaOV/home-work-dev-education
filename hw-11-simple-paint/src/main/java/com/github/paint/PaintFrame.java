package com.github.paint;

import javax.swing.*;
import java.awt.*;

public class PaintFrame extends JFrame {

    public PaintFrame(PaintPanel paintPanel, PaintFilePanel paintFilePanel, PaintLinePanel paintLinePanel) throws HeadlessException {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setBounds(100, 100, 1200, 800);
        add(paintPanel);
        add(paintFilePanel);
        add(paintLinePanel);
        setVisible(Boolean.TRUE);
    }

}
