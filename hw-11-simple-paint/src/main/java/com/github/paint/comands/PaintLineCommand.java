package com.github.paint.comands;

import com.github.paint.CustomLine;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PaintLineCommand {

    private CustomLine customLine;

    public ActionListener actionColor() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color color = JColorChooser.showDialog(null, "ff", Color.white);
                customLine.setColor(color);
            }
        };
    }
}
