package com.github.paint.comands;

import com.github.paint.CustomLine;
import com.github.paint.PaintPanel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

public class PaintFileCommands {

    private Gson gson = new Gson();

    private PaintPanel paintPanel;

    public void setPaintPanel(PaintPanel paintPanel) {
        this.paintPanel = paintPanel;
    }

    public ActionListener actionOpen() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();

                fileChooser.setDialogTitle("Выбор директории");
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Json", "json");
                FileNameExtensionFilter filter3 = new FileNameExtensionFilter(
                        "Word", "docx");
                fileChooser.setFileFilter(filter);
                fileChooser.addChoosableFileFilter(filter3);
                // Определение режима - только каталог
                fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                int resultFile = fileChooser.showOpenDialog(paintPanel);
                // Если директория выбрана, покажем ее в сообщении
                if (resultFile == JFileChooser.APPROVE_OPTION ) {
                    File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
                    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                        String str = br.readLine();
                        Type type = new TypeToken<List<CustomLine>>() {
                        }.getType();
                        List<CustomLine> result = gson.fromJson(str, type);
                        PaintFileCommands.this.paintPanel.setLines(result);
                        PaintFileCommands.this.paintPanel.repaint();
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        };
    }

    public ActionListener actionSave() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Сохранение файла");
                // Определение режима - только файл
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int result = fileChooser.showSaveDialog(paintPanel);
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Json", "json");
                FileNameExtensionFilter filter3 = new FileNameExtensionFilter(
                        "Word", "docx");
                fileChooser.setFileFilter(filter);
                fileChooser.addChoosableFileFilter(filter3);
                // Если файл выбран, то представим его в сообщении
                if (result == JFileChooser.APPROVE_OPTION ) {
                    try {
                        List<CustomLine> lines = PaintFileCommands.this.paintPanel.getLines();
                        String str = PaintFileCommands.this.gson.toJson(lines);
                        File file = new File(fileChooser.getSelectedFile().getAbsolutePath());
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
                            bw.write(str);
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        };
    }
}
