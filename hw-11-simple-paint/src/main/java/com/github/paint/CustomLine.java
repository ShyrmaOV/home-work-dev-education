package com.github.paint;

import java.awt.*;
import java.util.Objects;

public class CustomLine {

    private int firstX, firstY, lastX, lastY;

    private Color color = Color.BLACK;

    private int width;

    public CustomLine(int firstX, int firstY, int lastX, int lastY) {
        this.firstX = firstX;
        this.firstY = firstY;
        this.lastX = lastX;
        this.lastY = lastY;
        this.color = getColor();
        this.width = 1;
    }

    public int getFirstX() {
        return firstX;
    }

    public int getFirstY() {
        return firstY;
    }

    public int getLastX() {
        return lastX;
    }

    public int getLastY() {
        return lastY;
    }

    public Color getColor() {
        return color;
    }

    public int getWidth() {
        return width;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomLine that = (CustomLine) o;
        return firstX == that.firstX &&
                firstY == that.firstY &&
                lastX == that.lastX &&
                lastY == that.lastY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstX, firstY, lastX, lastY);
    }

    @Override
    public String toString() {
        return "CustomLine{" +
                "firstX=" + firstX +
                ", firstY=" + firstY +
                ", lastX=" + lastX +
                ", lastY=" + lastY +
                '}';
    }
}
