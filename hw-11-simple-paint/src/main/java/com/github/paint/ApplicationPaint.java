package com.github.paint;

import com.github.paint.comands.PaintFileCommands;
import com.github.paint.comands.PaintLineCommand;

import java.awt.*;

public class ApplicationPaint {
    public static void main(String[] args) {
        PaintFileCommands fileCommands = new PaintFileCommands();
        PaintLineCommand lineCommand = new PaintLineCommand();
        PaintPanel paintPanel = new PaintPanel();
        fileCommands.setPaintPanel(paintPanel);
        new PaintFrame(paintPanel, new PaintFilePanel(fileCommands), new PaintLinePanel(lineCommand))
                .getContentPane().setBackground(Color.BLACK);
    }
}
