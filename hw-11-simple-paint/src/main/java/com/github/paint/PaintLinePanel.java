package com.github.paint;

import com.github.paint.comands.PaintLineCommand;

import javax.swing.*;

public class PaintLinePanel extends JPanel {

    public PaintLinePanel(PaintLineCommand paintLineCommand) {
        setLayout(null);
        setBounds(820, 110, 90, 90);

        JButton btnColor = new JButton("Color");

        btnColor.addActionListener(paintLineCommand.actionColor());

        btnColor.setBounds(10, 10, 70, 30);

        add(btnColor);

        setVisible(Boolean.TRUE);
    }
}
