package com.github.paint;

import com.github.paint.comands.PaintFileCommands;

import javax.swing.*;

public class PaintFilePanel extends JPanel {

    public PaintFilePanel(PaintFileCommands paintFileCommands) {
        setLayout(null);
        setBounds(820, 10, 90, 90);

        JButton btnSave = new JButton("Save");
        JButton btnOpen = new JButton("Open");

        btnOpen.addActionListener(paintFileCommands.actionOpen());
        btnSave.addActionListener(paintFileCommands.actionSave());

        btnSave.setBounds(10, 10, 70, 30);
        btnOpen.setBounds(10, 50, 70, 30);

        add(btnSave);
        add(btnOpen);

        setVisible(Boolean.TRUE);
    }

}
