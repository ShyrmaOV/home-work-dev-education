package org.github.ilist;

import org.github.ilist.exeption.ListEmptyException;

public class LinkedListOnlyNext implements IList {

    private Node root;

    private static class Node {

        int value;

        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (init != null) {
            for (int i = 0; i < init.length; i++) {
                addEnd(init[i]);
            }
        }
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = this.root;
        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return count;
    }

    @Override
    public int[] toArray() {
        int size = size();
        if (size == 0) {
            return new int[0];
        }
        int count = 0;
        int[] array = new int[size];
        Node tmp = this.root;
        while (tmp != null) {
            array[count++] = tmp.value;
            tmp = tmp.next;
        }
        return array;
    }

    public String toString() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        String result = "";
        Node tmp = this.root;
        while (tmp.next != null) {
            result += tmp.value + ", ";
            tmp = tmp.next;
        }
        result += tmp.value;
        return result;
    }

    @Override
    public void addStart(int val) {
        if (this.root == null) {
            this.root = new Node(val);
        }
        else {
            Node tmp = new Node(val);
            tmp.next = this.root;
            this.root = tmp;
        }

    }

    @Override
    public void addEnd(int val) {
        if (this.root == null) {
            this.root = new Node(val);
        } else {
            Node tmp = this.root;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            tmp.next = new Node(val);
        }
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (pos < 0 || pos > size) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            addStart(val);
        } else if (pos == size) {
            addEnd(val);
        } else {
            Node tmp = this.root;
            Node newNode = new Node(val);
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            newNode.next = tmp.next;
            tmp.next = newNode;
        }
    }

    @Override
    public int removeStart() {
        Node tmp = this.root;
        int result;
        if (tmp != null) {
            result = this.root.value;
            this.root = tmp.next;
        } else {
            throw new ListEmptyException();
        }
        return result;

    }

    @Override
    public int removeEnd() {
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp.next != null) {
            tmp = tmp.next;
        }
        return tmp.value;
    }

    @Override
    public int removeByPos(int pos) {
        Node tmp = root;
        int result = 0;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            return removeStart();
        }
        int count = 0;
        Node newNode;
        while (tmp.next != null) {
            newNode = tmp;
            tmp = tmp.next;
            count++;
            if (count == pos) {
                result = tmp.value;
                newNode.next = tmp.next;
            }
        }
        return result;
    }

    @Override
    public int max() {
        int max = Integer.MIN_VALUE;
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            if (max < tmp.value) {
                max = tmp.value;
            }
            tmp = tmp.next;
        }
        return max;
    }

    @Override
    public int min() {
        int min = Integer.MAX_VALUE;
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            if (min > tmp.value) {
                min = tmp.value;
            }
            tmp = tmp.next;
        }
        return min;
    }

    @Override
    public int maxPos() {
        int max = Integer.MIN_VALUE;
        int index = 0;
        int count = 0;
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            if (max < tmp.value) {
                max = tmp.value;
                index = count;
            }
            count++;
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int minPos() {
        int min = Integer.MAX_VALUE;
        int index = 0;
        int count = 0;
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            if (min > tmp.value) {
                min = tmp.value;
                index = count;
            }
            count++;
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int[] sort() {
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            Node secondNode = tmp.next;
            while (secondNode != null) {
                if (tmp.value > secondNode.value) {
                    int interSpace = tmp.value;
                    tmp.value = secondNode.value;
                    secondNode.value = interSpace;
                }
                secondNode = secondNode.next;
            }
            tmp = tmp.next;
        }
        return toArray();
    }

    @Override
    public int get(int pos) {
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            return root.value;
        }
        int count = 0;
        int result = 0;
        while (tmp.next != null) {
            count++;
            tmp = tmp.next;
            if (count == pos) {
                result = tmp.value;
            }
        }
        return result;
    }

    @Override
    public int[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] newArray = new int[size];
        final int len = size / 2;
        final int offset = size - len;
        for (int i = 0; i < len; i++) {
            int temp = newArray[i];
            newArray[i] = newArray[offset + i];
            newArray[offset + i] = temp;
        }
        return newArray;
    }

    @Override
    public int[] revers() {
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        Node prev = null;
        Node next;
        while (tmp != null) {
            next = tmp.next;
            tmp.next = prev;
            prev = tmp;
            tmp = next;
        }
        root = prev;
        return toArray();
    }

    @Override
    public void set(int pos, int val) {
        Node tmp = root;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            root.value = val;
            return;
        }
        int count = 0;
        while (tmp.next != null) {
            count++;
            tmp = tmp.next;
            if (count == pos) {
                tmp.value = val;
                return;
            }
        }
    }
}
