package org.github.ilist;

import org.github.ilist.exeption.ListEmptyException;

import java.util.Arrays;

public class ArrayListCapacity implements IList {

    private int[] array = new int[0];

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.array = new int[0];
        } else {
            this.array = new int[init.length];
            System.arraycopy(init, 0, this.array, 0, init.length);
        }
    }

    @Override
    public void clear() {
        this.array = new int[0];
    }


    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public int[] toArray() {
        int size = size();
        int[] result = new int[size];
        System.arraycopy(this.array, 0, result, 0, size);
        return result;
    }

    @Override
    public String toString() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            if (i != size() - 1) {
                str.append(this.array[i]).append(", ");
            } else str.append(this.array[i]);
        }
        return str.toString();
    }

    @Override
    public void addStart(int val) {
        int size = size();
        int[] tmp = new int[size + 1];
        if (size >= 0) System.arraycopy(this.array, 0, tmp, 1, size);
        tmp[0] = val;
        this.array = tmp;
    }

    @Override
    public void addEnd(int val) {
        int size = size();
        int[] tmp = new int[size + 1];
        if (size >= 0) System.arraycopy(this.array, 0, tmp, 0, size);
        tmp[size] = val;
        this.array = tmp;

    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if ((pos < 0) || (pos > size)) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size + 1];
        int count = 0;
        for (int i = 0; i < tmp.length; i++) {
            if (i == pos) {
                tmp[i] = val;
            } else {
                tmp[i] = this.array[count];
                count++;
            }
        }
        this.array = tmp;
    }

    @Override
    public int removeStart() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        int deleted = array[0];
        System.arraycopy(this.array, 1, tmp, 0, tmp.length);
        this.array = tmp;

        return deleted;
    }

    @Override
    public int removeEnd() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        int deleted = array[size - 1];
        System.arraycopy(this.array, 0, tmp, 0, tmp.length);
        this.array = tmp;

        return deleted;
    }

    @Override
    public int removeByPos(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        int deleted = array[pos];
        int count = 0;
        for (int i = 0; i < tmp.length; i++) {
            if (i == pos) {
                count++;
            }
            tmp[i] = this.array[count];
            count++;
        }
        this.array = tmp;

        return deleted;
    }

    @Override
    public int max() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        for (int i = 1; i < size(); i++)
            if (max < this.array[i])
                max = this.array[i];
        return max;
    }

    @Override
    public int min() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        int min = array[0];
        for (int i = 1; i < array.length; i++)
            if (min > array[i])
                min = array[i];
        return min;
    }

    @Override
    public int maxPos() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        int max = array[0];
        int maxIndex = 0;
        for (int i = 1; i < array.length; i++)
            if (max < array[i]) {
                max = array[i];
                maxIndex = i;
            }
        return maxIndex;
    }

    @Override
    public int minPos() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        int min = array[0];
        int minIndex = 0;
        for (int i = 1; i < array.length; i++)
            if (min > array[i]) {
                min = array[i];
                minIndex = i;
            }

        return minIndex;
    }

    @Override
    public int[] sort() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        Arrays.sort(this.array);
        return this.array;
    }

    @Override
    public int get(int pos) {
        if (this.size() == 0 || pos < 0 || pos >= size()) {
            throw new ListEmptyException();
        }
        return array[pos];
    }

    @Override
    public int[] halfRevers() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        if (this.size() == 1) {
            return this.array;
        } else {
            int j = 0;
            final int len = size() / 2;
            final int offset = size() - len;
            int[] newArray = new int[size()];
            for (int i = 0; i < size(); i++) {
                if (i < len) {
                    newArray[i + len] = this.array[i];
                } else {
                    newArray[i - offset] = this.array[i];
                }
                j++;

        }
        this.array = newArray;
        return this.array;
        }
    }

    @Override
    public int[] revers() {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        int j = 0;
        int[] reverseArray = new int[size()];
        for (int i = size() - 1; i >= 0; i--) {
            reverseArray[j] = array[i];
            j++;
        }
        return reverseArray;
    }

    @Override
    public void set(int pos, int val) {
        if (this.size() == 0) {
            throw new ListEmptyException();
        }
        this.array[pos] = val;
    }

}
