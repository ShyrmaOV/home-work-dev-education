package org.bitbucket.list;

import org.bitbucket.list.exeption.ListEmptyException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArrayListGenericTest {

    private IListGeneric<Integer> listGen = new ArrayListGeneric<>();

    //==============================================================
    //======================= Clear ================================
    //==============================================================

    @Test
    public void clearMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.clear();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.clear();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.clear();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }


    @Test
    public void clearZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.clear();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    //==============================================================
    //======================== Size ================================
    //==============================================================

    @Test
    public void sizeMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 10;
        int act = this.listGen.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        int exp = 2;
        int act = this.listGen.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 1;
        int act = this.listGen.size();
        assertEquals(exp, act);
    }


    @Test
    public void sizeZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.size();
        assertEquals(exp, act);
    }

    //==============================================================
    //======================= To array =============================
    //==============================================================

    @Test
    public void toArrayMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        Integer[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void toArrayTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        Integer[] exp = {0, 1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void toArrayOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        Integer[] exp = {0};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void toArrayZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    //==============================================================
    //======================= Add Start ============================
    //==============================================================

    @Test
    public void addStartMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.addStart(10);
        Integer[] exp = {10, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.addStart(10);
        Integer[] exp = {10, 0, 1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.addStart(10);
        Integer[] exp = {10, 0};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.addStart(10);
        Integer[] exp = {10};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    //==============================================================
    //========================= Add End ============================
    //==============================================================

    @Test
    public void addEndMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.addEnd(10);
        Integer[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.addEnd(10);
        Integer[] exp = {0, 1, 10};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.addEnd(10);
        Integer[] exp = {0, 10};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.addEnd(10);
        Integer[] exp = {10};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    //==============================================================
    //========================= Add By Pos =========================
    //==============================================================

    @Test
    public void addByPosMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.addByPos(2, 5);
        Integer[] exp = {0, 1, 5, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.addByPos(0, 3);
        Integer[] exp = {3, 0, 1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.addByPos(0, 1);
        Integer[] exp = {1, 0};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void addByPosZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.addByPos(0, 1);
    }

    @Test(expected = ListEmptyException.class)
    public void addByPosMore() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.addByPos(3, 1);
    }

    @Test(expected = ListEmptyException.class)
    public void addByPosOdd() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.addByPos(-1, 1);
    }

    //==============================================================
    //======================== Remove Start ========================
    //==============================================================

    @Test
    public void removeStartMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.removeStart();
        Integer[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.removeStart();
        Integer[] exp = {1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.removeStart();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void removeStartZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.removeStart();
    }

    //==============================================================
    //========================= Remove End =========================
    //==============================================================

    @Test
    public void removeEndMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.removeEnd();
        Integer[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.removeEnd();
        Integer[] exp = {0};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.removeEnd();
        Integer[] exp = {};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void removeEndZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.removeEnd();
    }

    //==============================================================
    //============================ Max =============================
    //==============================================================

    @Test
    public void maxMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 9;
        int act = this.listGen.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        int exp = 1;
        int act = this.listGen.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.max();
        assertEquals(exp, act);
    }


    @Test(expected = ListEmptyException.class)
    public void maxZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.max();
    }

    //==============================================================
    //============================ Min =============================
    //==============================================================

    @Test
    public void minMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.min();
        assertEquals(exp, act);
    }

    @Test
    public void minTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.min();
        assertEquals(exp, act);
    }

    @Test
    public void minOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.min();
        assertEquals(exp, act);
    }


    @Test(expected = ListEmptyException.class)
    public void minZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.min();
    }

    //==============================================================
    //========================== Max Pos ===========================
    //==============================================================

    @Test
    public void maxPosMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 9;
        int act = this.listGen.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosTwo() {
        Integer[] array = new Integer[]{1, 0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.maxPos();
        assertEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void maxPosZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.maxPos();
    }

    //==============================================================
    //========================== Min Pos ===========================
    //==============================================================

    @Test
    public void minPosMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosTwo() {
        Integer[] array = new Integer[]{1, 0};
        this.listGen.init(array);
        int exp = 1;
        int act = this.listGen.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.minPos();
        assertEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void minPosZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.minPos();
    }

    //==============================================================
    //=========================== Sort =============================
    //==============================================================

    @Test
    public void sortMany() {
        Integer[] array = new Integer[]{0, 2, 1, 3, 5, 4, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.sort();
        Integer[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        Integer[] array = new Integer[]{2, 1};
        this.listGen.init(array);
        this.listGen.sort();
        Integer[] exp = {1, 2};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }


    @Test
    public void sortOne() {
        Integer[] array = new Integer[]{1};
        this.listGen.init(array);
        this.listGen.sort();
        Integer[] exp = {1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void sortZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.sort();
    }

    //==============================================================
    //=========================== Get ==============================
    //==============================================================

    @Test
    public void getMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        int exp = 5;
        int act = this.listGen.get(5);
        assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        int exp = 1;
        int act = this.listGen.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        int exp = 0;
        int act = this.listGen.get(0);
        assertEquals(exp, act);
    }


    @Test(expected = ListEmptyException.class)
    public void getZero() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.get(20);

    }

    @Test(expected = ListEmptyException.class)
    public void getMore() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.get(0);

    }

    //==============================================================
    //======================= Half Revers ==========================
    //==============================================================

    @Test
    public void halfReversMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        Integer[] exp = {5, 6, 7, 8, 9, 0, 1, 2, 3, 4};
        Object[] act = this.listGen.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        Integer[] exp = {1, 0};
        Object[] act = this.listGen.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        Integer[] exp = {0};
        Object[] act = this.listGen.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void halfReversZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.halfRevers();
    }

    //==============================================================
    //========================== Revers ============================
    //==============================================================

    @Test
    public void reversMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        Integer[] exp = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        Object[] act = this.listGen.revers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        Integer[] exp = {1, 0};
        Object[] act = this.listGen.revers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        Integer[] exp = {0};
        Object[] act = this.listGen.revers();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void reversZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.revers();
    }

    //==============================================================
    //=========================== Set ==============================
    //==============================================================

    @Test
    public void setMany() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.set(2, 5);
        Integer[] exp = {0, 1, 5, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        Integer[] array = new Integer[]{0, 1};
        this.listGen.init(array);
        this.listGen.set(0, 5);
        Integer[] exp = {5, 1};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        Integer[] array = new Integer[]{0};
        this.listGen.init(array);
        this.listGen.set(0, 5);
        Integer[] exp = {5};
        Object[] act = this.listGen.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void setZero() {
        Integer[] array = new Integer[]{};
        this.listGen.init(array);
        this.listGen.set(0, 5);
    }

    @Test(expected = ListEmptyException.class)
    public void setMore() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.set(15, 5);
    }

    @Test(expected = ListEmptyException.class)
    public void setOdd() {
        Integer[] array = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        this.listGen.init(array);
        this.listGen.set(-1, 5);
    }

}