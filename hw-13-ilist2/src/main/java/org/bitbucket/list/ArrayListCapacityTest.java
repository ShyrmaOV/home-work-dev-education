package org.bitbucket.list;

import org.bitbucket.list.exeption.ListEmptyException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ArrayListCapacityTest {

    private final String name;

    private final IList list;

    public ArrayListCapacityTest(String name, IList list) {
        this.name = name;
        this.list = list;
    }

    @Before
    public void setUp() {
        this.list.clear();
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object[]> instances() {
        return Arrays.asList(new Object[][] {
                {"Array List Ten Capacity", new ArrayListTenCapacity()},
                {"Linked List Next And Prev", new LinkedListNextAndPrev()}
        });
    }

    //=================================================
    //=================== Init ========================
    //=================================================

    @Test
    public void initMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int[] exp = {1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int[] exp = {1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int[] exp = {1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initZero() {
        int[] array = {};
        this.list.init(array);
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Clean ========================
    //=================================================

    @Test
    public void clearMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearZero() {
        int[] array = {};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 10;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 2;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== To Array ========================
    //=================================================

    @Test
    public void toArrayMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 10;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void toArrayTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 2;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void toArrayOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void toArrayZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== To String ===================
    //=================================================

    @Test
    public void toStringMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        String exp = "1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12";
        String act = this.list.toString();
        assertEquals(exp, act);
    }

    @Test
    public void toStringTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        String exp = "1, 232";
        String act = this.list.toString();
        assertEquals(exp, act);
    }

    @Test
    public void toStringOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        String exp = "1";
        String act = this.list.toString();
        assertEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void toStringZero() {
        int[] array = new int[]{};
        this.list.init(array);
        String exp = "";
        String act = this.list.toString();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== Add Start ===================
    //=================================================

    @Test
    public void addStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        this.list.init(array);
        this.list.addStart(2);
        int[] exp = {2, 1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addStart(2);
        int[] exp = {2, 1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addStart(2);
        int[] exp = {2, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addStart(2);
    }

    //=================================================
    //=================== Add End =====================
    //=================================================

    @Test
    public void addEndMany() {
        this.list.clear();
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        this.list.init(array);
        this.list.addEnd(2);
        int[] exp = {1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addEnd(2);
        int[] exp = {1, 232, 2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addEnd(2);
        int[] exp = {1, 2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addEnd(2);
        int[] exp = {2};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Add By Pos ===================
    //=================================================

    @Test
    public void addByPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        this.list.init(array);
        this.list.addByPos(2, 3);
        int[] exp = {1, 232, 3, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] act = this.list.toArray();
        System.out.println(Arrays.toString(this.list.toArray()));
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addByPos(1, 3);
        int[] exp = {1, 3, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addByPos(1, 3);
        int[] exp = {1, 3};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addByPos(0, 3);
        int[] exp = {3};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void addByPosIncorrect() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addByPos(45, 3);
    }

    //=================================================
    //================== Remove Start ===================
    //=================================================

    @Test
    public void removeStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 1;
        int act = this.list.removeStart();
        int[] arrExp = {232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.removeStart();
        int[] arrExp = {232};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.removeStart();
        int[] arrExp = {};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test(expected = ListEmptyException.class)
    public void removeStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.removeStart();
        int[] arrExp = {};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);

    }

    //=================================================
    //================== Remove End ===================
    //=================================================

    @Test
    public void removeEndMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 12;
        int act = this.list.removeEnd();
        int[] arrExp = {1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.removeEnd();
        int[] arrExp = {1};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.removeEnd();
        int[] arrExp = {};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test(expected = ListEmptyException.class)
    public void removeEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.removeEnd();
        int[] arrExp = {};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    //=================================================
    //================== Remove By Pos ===================
    //=================================================

    @Test
    public void removeByPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        this.list.init(array);
        int exp = 232;
        int act = this.list.removeByPos(1);
        int[] arrExp = {1, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeByPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.removeByPos(1);
        int[] arrExp = {1};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test
    public void removeByPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.removeByPos(0);
        int[] arrExp = {};
        int[] arrAct = this.list.toArray();
        assertEquals(exp, act);
        assertArrayEquals(arrExp, arrAct);
    }

    @Test(expected = ListEmptyException.class)
    public void removeByPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeByPos(0);
    }

    //=================================================
    //================== Max ===================
    //=================================================

    @Test
    public void maxMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 43432;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void maxZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.max();

    }

    //=================================================
    //=================== Min =====================
    //=================================================

    @Test
    public void minMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void minZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.min();

    }

    //=================================================
    //=================== MaxPos =====================
    //=================================================

    @Test
    public void maxPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 2;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 0;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void maxPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.maxPos();

    }

    //=================================================
    //=================== minPos =====================
    //=================================================

    @Test
    public void minPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void minPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.minPos();

    }

    //=================================================
    //=================== Sort ========================
    //=================================================

    @Test
    public void sortMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 12, 34, 123, 123, 232, 543, 4343, 5644, 43432};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        int[] array = new int[]{232, 1};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void sortZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Get =========================
    //=================================================

    @Test
    public void getMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 123;
        int act = this.list.get(3);
        assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.get(0);
        assertEquals(exp, act);
    }


    @Test(expected = ListEmptyException.class)
    public void getZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.get(0);
    }

    @Test(expected = ListEmptyException.class)
    public void getIncorrect() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.get(10);
    }

    //=================================================
    //================= HalfRevers ====================
    //=================================================

    @Test
    public void halfReversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int[] exp = new int[]{4343, 123, 5644, 34, 12, 1, 232, 43432, 123, 543};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int[] exp = new int[]{232, 1};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int[] exp = new int[]{1};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void halfReversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int[] exp = new int[]{};
        int[] act = this.list.halfRevers();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //================= halfRevers ====================
    //=================================================

    @Test
    public void reversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int[] exp = new int[]{12, 34, 5644, 123, 4343, 543, 123, 43432, 232, 1};
        int[] act = this.list.revers();
        System.out.println();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int[] exp = new int[]{232, 1};
        int[] act = this.list.revers();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test (expected = ListEmptyException.class)
    public void reversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.revers();
    }

    //=================================================
    //=================== set =====================
    //=================================================

    @Test
    public void setMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.set(3, 1000);
        int[] exp = {1, 232, 43432, 1000, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.set(1, 1000);
        int[] exp = {1, 1000};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.set(0, 1000);
        int[] exp = {1000};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }


    @Test(expected = ListEmptyException.class)
    public void setZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.set(0, 10);
    }

//    @Test(expected = ListEmptyException.class)
//    public void setIncorrect() {
//        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
//        this.list.init(array);
//        this.list.set(10, 1);
//    }
}