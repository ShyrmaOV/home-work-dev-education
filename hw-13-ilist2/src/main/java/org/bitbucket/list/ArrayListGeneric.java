package org.bitbucket.list;

import org.bitbucket.list.exeption.ListEmptyException;

public class ArrayListGeneric <T extends Comparable<T>> implements IListGeneric<T> {

    private T[] array = (T[]) new Comparable[10];

    private int index;

    @Override
    public void init(T[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public <T> T[] toArray() {
        Object[] result = new Object[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = array[i];
        }
        return (T[]) result;
    }

    @Override
    public void addStart(T val) {
        int size = size();
        T[] tmp = (T[]) new Comparable[size + 1];
        for (int i = this.index; i > 0; i--) {
            tmp[i] = (T) this.array[i - 1];
        }
        tmp[0] = val;
        this.index++;
        this.array = tmp;
    }

    @Override
    public void addStart(T val, Comparable<?> com) {

    }

    @Override
    public void addEnd(T val) {
        int size = size();
        T[] tmp = (T[]) new Comparable[size + 1];
        for (int i = 0; i < size; i++) {
            tmp[i] = (T) this.array[i];
        }
        tmp[size] = val;
        this.index++;
        this.array = tmp;
    }

    @Override
    public void addByPos(int pos, T val) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new ListEmptyException();
        }
        this.index++;
        T[] tmp = (T[]) new Comparable[this.index];
        for (int i = 0; i < pos; i++) {
            tmp[i] = (T) this.array[i];
        }
        tmp[pos] = val;
        for (int i = pos + 1; i < this.index; i++) {
            tmp[i] = (T) this.array[i - 1];
        }
        this.array = tmp;
    }

    @Override
    public T removeStart() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        this.index--;
        T[] tmp = (T[]) new Comparable[this.index];
        T result = (T) this.array[0];
        for (int i = 0; i < this.index; i++) {
            tmp[i] = (T) this.array[i + 1];
        }
        this.array = tmp;
        return result;
    }

    @Override
    public T removeEnd() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        this.index--;
        T[] tmp = (T[]) new Comparable[this.index];
        T result = (T) this.array[this.index];
        for (int i = 0; i < this.index; i++) {
            tmp[i] = (T) this.array[i];
        }
        this.array = tmp;
        return result;
    }

    @Override
    public T removeByPos(int pos) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= this.index) {
            throw new ListEmptyException();
        }
        this.index--;
        T[] tmp = (T[]) new Comparable[this.index];
        T result = (T) this.array[pos];
        for (int i = 0; i < this.index; i++) {
            if (pos <= i) {
                tmp[i] = (T) this.array[i + 1];
            } else {
                tmp[i] = (T) this.array[i];
            }
        }
        this.array = tmp;
        return result;
    }

    @Override
    public T max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T max = this.array[0];
        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(max) > 0) {
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public T min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T min = this.array[0];
        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(min) < 0) {
                min = this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(this.array[result]) > 0)
                result = i;
        }
        return result;
    }

    @Override
    public int minPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int result = 0;

        for (int i = 1; i < size; i++) {
            if (this.array[i].compareTo(this.array[result]) < 0)
                result = i;
        }
        return result;
    }

    @Override
    public T[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        for (int i = 1; i < size; i++) {
            for (int j = i; j > 0; j--) {
                if (this.array[j].compareTo(this.array[j - 1]) < 0) {
                    T tmp = this.array[j];
                    this.array[j] = this.array[j - 1];
                    this.array[j - 1] = tmp;
                }
            }
        }
        return this.array;
    }

    @Override
    public T get(int pos) {
        if (pos < 0 || pos >= size()) {
            throw new ListEmptyException();
        }
        return this.array[pos];
    }

    @Override
    public T[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] result = (T[]) new Comparable[size];
        int half = size / 2;
        for (int i = half; i < size; i++) {
            result[i - half] = this.array[i];
        }
        for (int i = 0; i < half; i++) {
            result[i + size - half] = this.array[i];
        }
        return result;
    }

    @Override
    public T[] revers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        T[] result = (T[]) new Comparable[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.array[size - 1 - i];
        }
        return result;
    }

    @Override
    public void set(int pos, T val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos >= size) {
            throw new ListEmptyException();
        }
        T[] tmp = (T[]) new Comparable[size];
        for (int i = 0; i < size; i++) {
            if (pos == i) {
                tmp[i] = val;
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
    }
}
