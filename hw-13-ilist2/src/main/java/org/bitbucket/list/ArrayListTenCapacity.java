package org.bitbucket.list;

import org.bitbucket.list.exeption.ListEmptyException;

public class ArrayListTenCapacity implements IList {

    private int[] array = new int[10];

    private int index;

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public void addStart(int val) {
        for (int i = this.index; i > 0 ; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = val;
        this.index++;
    }

    @Override
    public void addEnd(int val) {
        this.array[this.index] = val;
        this.index++;
    }

    @Override
    public void addByPos(int pos, int val) {
        if (pos < 0 || pos > size()) {
            throw new ListEmptyException();
        }
        this.index++;
        int count = this.index-1;
        for (int i = this.index; i > 0 ; i--) {
            if (pos == this.index) {
                this.array[i] = val;
            }
            else {
                this.array[i] = this.array[count];
                count--;
            }
        }

    }

    @Override
    public int removeStart() {
        if (this.index < 1) {
            throw new ListEmptyException();
        }
        int result = array[0];
        for (int i = 0; i < this.index - 1; i++) {
            this.array[i] = this.array[i + 1];
        }
        this.index--;
        return result;

    }

    @Override
    public int removeEnd() {
        if (this.index < 1) {
            throw new ListEmptyException();
        }
        int el = this.array[this.index - 1];
        this.index--;
        return el;

    }

    @Override
    public int removeByPos(int pos) {
        if (this.index == 0 || pos < 0 || pos > this.index) {
            throw new ListEmptyException();
        }
        int result = this.array[pos];
        index--;
        int[] tmp = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            if (pos <= i) {
                tmp[i] = this.array[i + 1];
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
        return result;
    }

    @Override
    public int max() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        for (int i = 1; i < this.index; i++) {
            if (max < this.array[i]) {
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public int min() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        for (int i = 1; i < this.index; i++) {
            if (min > this.array[i]) {
                min = this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        int result = 0;
        for (int i = 1; i < this.index; i++) {
            if (max < this.array[i]) {
                max = this.array[i];
                result = i;
            }
        }
        return result;
    }

    @Override
    public int minPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        int result = 0;
        for (int i = 1; i < this.index; i++) {
            if (min > this.array[i]) {
                min = this.array[i];
                result = i;
            }
        }
        return result;
    }

    @Override
    public int[] sort() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < this.index - 1; i++) {
                if (array[i] > array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    sorted = false;
                }
            }
        }
        return this.array;
    }

    @Override
    public int get(int pos) {
        if (this.index == 0 || pos < 0 || pos >= this.index) {
            throw new ListEmptyException();
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        if (index == 0) {
            throw new ListEmptyException();
        }
        int half;
        if (index % 2 == 0) {
            half = index / 2;
        } else {
            half = (index + 1) / 2;
        }

        int tmp = index - half;
        for (int i = 0; i < half; i++) {
            int temp = this.array[i];
            this.array[i] = this.array[tmp + i];
            this.array[tmp + i] = temp;
        }
        return this.array;
    }

    @Override
    public int[] revers() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int tmp;
        for (int i = 0; i < index / 2; i++) {
            tmp = this.array[i];
            this.array[i] = this.array[index - 1 - i];
            array[this.index - 1 - i] = tmp;
        }
        return this.array;
    }

    @Override
    public void set(int pos, int val) {
        if (pos > index || pos < 0 || this.index == 0) {
            throw new ListEmptyException();
        }
        this.array[pos] = val;
    }

}
