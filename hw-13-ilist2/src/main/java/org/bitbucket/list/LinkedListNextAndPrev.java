package org.bitbucket.list;

import org.bitbucket.list.exeption.ListEmptyException;

public class LinkedListNextAndPrev  implements IList {

    private Node start;

    private Node end;

    private static class Node {

        int value;

        Node next;

        Node prev;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (init != null) {
            for (int i = init.length - 1; i >= 0; i--) {
                addStart(init[i]);
            }
        }
    }

    @Override
    public void clear() {
        this.start = null;
        this.end = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = this.start;
        while (tmp != null) {
            count++;
            tmp = tmp.next;
        }
        return count;
    }

    @Override
    public int[] toArray() {
        int size = size();
        if (size == 0) {
            return new int[0];
        }
        int count = 0;
        int[] array = new int[size];
        Node tmp = this.start;
        while (tmp != null) {
            array[count++] = tmp.value;
            tmp = tmp.next;
        }
        return array;
    }

    @Override
    public void addStart(int val) {
        Node tmp = new Node(val);
        tmp.next = this.start;
        this.start = tmp;
    }

    @Override
    public void addEnd(int val) {
        if (this.start == null) {
            this.start = this.end = new Node(val);
        } else {
            Node tmp = this.start;
            while (tmp.next != null) {
                tmp = tmp.next;
            }
            tmp.next = new Node(val);
        }
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (pos < 0 || pos > size) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            addStart(val);
        } else if (pos == size) {
            addEnd(val);
        } else {
            Node tmp = this.start;
            Node newNode = new Node(val);
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            newNode.next = tmp.next;
            tmp.next = newNode;
        }
    }

    @Override
    public int removeStart() {
        Node tmp = this.start;
        if (tmp == null) {
            throw new ListEmptyException();
        } else {
            Node newNode = tmp.next;
            int result = tmp.value;
            start = newNode;
            return result;
        }
    }

    @Override
    public int removeEnd() {
        Node tmp = start;
        if (tmp == null ) {
            throw new ListEmptyException();
        }
        while (tmp.next != null) {
            tmp = tmp.next;
        }
        return tmp.value;
    }

    @Override
    public int removeByPos(int pos) {
        Node tmp = start;
        int result = 0;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            return removeStart();
        }
        int count = 0;
        Node newNode;
        while (tmp.next != null) {
            newNode = tmp;
            tmp = tmp.next;
            count++;
            if (count == pos) {
                result = tmp.value;
                newNode.next = tmp.next;
            }
        }
        return result;
    }

    @Override
    public int max() {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        int max = this.start.value;
        while (tmp != null) {
            if (max < tmp.value) {
                max = tmp.value;
            }
            tmp = tmp.next;
        }
        return max;
    }

    @Override
    public int min() {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        int min = this.start.value;
        while (tmp != null) {
            if (min > tmp.value) {
                min = tmp.value;
            }
            tmp = tmp.next;
        }
        return min;
    }

    @Override
    public int maxPos() {
        int index = 0;
        int count = 0;
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        int max = this.start.value;
        while (tmp != null) {
            if (max < tmp.value) {
                max = tmp.value;
                index = count;
            }
            count++;
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int minPos() {
        int index = 0;
        int count = 0;
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        int min = this.start.value;
        while (tmp != null) {
            if (min > tmp.value) {
                min = tmp.value;
                index = count;
            }
            count++;
            tmp = tmp.next;
        }
        return index;
    }

    @Override
    public int[] sort() {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        while (tmp != null) {
            Node secondNode = tmp.next;
            while (secondNode != null) {
                if (tmp.value > secondNode.value) {
                    int interSpace = tmp.value;
                    tmp.value = secondNode.value;
                    secondNode.value = interSpace;
                }
                secondNode = secondNode.next;
            }
            tmp = tmp.next;
        }
        return toArray();
    }

    @Override
    public int get(int pos) {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            return start.value;
        }
        int result = 0;
        int count = 0;
        while (tmp.next != null) {
            count++;
            tmp = tmp.next;
            if (count == pos) {
                result = tmp.value;
            }
        }
        return result;
    }

    @Override
    public int[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int len;
        if (size % 2 == 0) {
            len = size / 2;
        } else {
            len = (size + 1) / 2;
        }
        for (int i = 0; i < len; i++) {
            Node tmp = this.start;
            removeStart();
            addEnd(tmp.value);
        }
        return toArray();
    }

    @Override
    public int[] revers() {
        Node tmp = start;
        if (tmp == null) {
            throw new ListEmptyException();
        }
        Node prev = null;
        Node next;
        while (tmp != null) {
            next = tmp.next;
            tmp.next = prev;
            prev = tmp;
            tmp = next;
        }
        start = prev;
        return toArray();
    }

    @Override
    public void set(int pos, int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        if (pos < 0 || pos > size) {
            throw new ListEmptyException();
        }
        if (pos == 0) {
            this.start.value = val;
        } else {
            Node tmp = this.start;
            for (int i = 0; i < pos - 1; i++) {
                tmp = tmp.next;
            }
            tmp.next.value = val;
        }
    }
}
