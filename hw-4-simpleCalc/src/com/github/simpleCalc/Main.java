package com.github.simpleCalc;

public class Main {
    public static void main(String[] args) {
        CalcGUI app = new CalcGUI(new ActionManager());
        app.setVisible(true);
    }
}
