package com.github.simpleCalc;

class MathCalc {
    public static float calculate(float a, float b, String op) {
        float result = 0;
        switch (op) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
            default:
                return 0;
        }
        return result;
    }
}
