package com.github.simpleCalc;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionManager {
    private JTextField firstFunctionField;
    private JTextField secondFunctionField;
    private JTextField operationField;
    private JTextField resultField;

    private final ButtonActionListener buttonActionListener = new ButtonActionListener();

    private class ButtonActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            float firstNumber = Float.parseFloat(firstFunctionField.getText());
            float secondNumber = Float.parseFloat(secondFunctionField.getText());
            String operation = operationField.getText();
            float result = MathCalc.calculate(firstNumber, secondNumber, operation);
            resultField.setText(String.valueOf(result));

        }
    }
    public ButtonActionListener getButtonActionListener() {
        return buttonActionListener;
    }
    public void setFirstFunctionField(JTextField firstFunctionField) {
        this.firstFunctionField = firstFunctionField;
    }

    public void setSecondFunctionField(JTextField secondFunctionField) {
        this.secondFunctionField = secondFunctionField;
    }

    public void setOperationField(JTextField operationField) {
        this.operationField = operationField;
    }

    public void setResultField(JTextField resultField) {
        this.resultField = resultField;
    }
}
