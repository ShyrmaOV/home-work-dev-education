package com.github.simpleCalc;

import javax.swing.*;
import java.awt.*;

public class CalcGUI extends JFrame {
    private JTextField firstNumberField;
    private JTextField operationField;
    private JTextField secondNumberField;
    private JTextField resultField;

    private JLabel firstNumberLabel;
    private JLabel operationLabel;
    private JLabel secondNumberLabel;
    private JLabel resultLabel;

    public CalcGUI(ActionManager ac) {
        super("Simple Calculator");
        JPanel content = new JPanel();
        content.setLayout(null);
        content.setBackground(new Color(221,226,206));
        setSize(360, 370);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        firstNumberLabel = new JLabel("Число 1");
        firstNumberLabel.setBounds(20, 30, 100, 40);
        operationLabel = new JLabel("Операция");
        operationLabel.setBounds(10, 130, 100, 40);
        secondNumberLabel = new JLabel("Число 2");
        secondNumberLabel.setBounds(20, 80, 100, 40);
        resultLabel= new JLabel("Результат");
        resultLabel.setBounds(10, 265, 100, 40);

        firstNumberField = new JTextField(20);
        firstNumberField.setBounds(80, 30, 245, 40);
        operationField = new JTextField(20);
        operationField.setBounds(80, 130, 245, 40);
        secondNumberField = new JTextField(20);
        secondNumberField.setBounds(80, 80, 245, 40);
        resultField = new JTextField(20);
        resultField.setBounds(80, 265, 245, 40);

        JButton btnCalc = new JButton();
        btnCalc.setBounds(15, 190, 310, 57);
        ImageIcon icon =new ImageIcon("C:/Home-work/home-work-dev-education/image/buttonEquals.png");
        Image im = icon.getImage();
        Image im2 = im.getScaledInstance(310, 57, Image.SCALE_SMOOTH);
        btnCalc.setIcon(new ImageIcon(im2));

        ac.setFirstFunctionField(this.firstNumberField);
        ac.setSecondFunctionField(this.secondNumberField);
        ac.setOperationField(this.operationField);
        ac.setResultField(this.resultField);
        btnCalc.addActionListener(ac.getButtonActionListener());

        content.add(firstNumberLabel);
        content.add(operationLabel);
        content.add(secondNumberLabel);
        content.add(firstNumberField);
        content.add(secondNumberField);
        content.add(operationField);
        content.add(btnCalc);
        content.add(resultLabel);
        content.add(resultField);

        setContentPane(content);
    }
}
