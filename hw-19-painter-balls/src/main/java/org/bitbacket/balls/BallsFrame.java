package org.bitbacket.balls;

import javax.swing.*;
import java.awt.*;

public class BallsFrame extends JFrame {

    public BallsFrame() throws HeadlessException {
        setLayout(null);
        setSize(800, 600);
        BallsPanel ballsPanel = new BallsPanel();
        ballsPanel.setBounds(10, 10, 765, 545);
        add(ballsPanel);
        setVisible(Boolean.TRUE);
    }
}
