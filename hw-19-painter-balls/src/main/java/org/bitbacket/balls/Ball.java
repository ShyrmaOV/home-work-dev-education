package org.bitbacket.balls;

import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private Logger log = Logger.getLogger(Ball.class);

    private Random rnd = new Random();

    private int deltaX;

    private int deltaY;

    private Color color;

    private Point point;

    private int ballSize;

    JPanel pan;

    public Ball(Point point) {
        this.point = point;
        this.color = new Color(this.rnd.nextInt(255), this.rnd.nextInt(255), this.rnd.nextInt(255));
        this.deltaX = this.rnd.nextInt(10) + 5;
        this.deltaY = this.rnd.nextInt(10) + 5;
        this.ballSize = this.rnd.nextInt(50);
        setSize(this.ballSize, this.ballSize);
        setOpaque(Boolean.FALSE);
    }

    private void move() {
        pan = (JPanel) getParent();
        if (this.point.x <= 0 || this.point.x + this.ballSize >= pan.getWidth()) {
            deltaX = -deltaX;
        }
        if (this.point.y <= 0 || this.point.y + this.ballSize >= pan.getHeight()) {
            deltaY = -deltaY;
        }
        this.point.translate(deltaX, deltaY);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(color);
        g2d.fillOval(1, 1, this.ballSize, this.ballSize);
    }

    @Override
    public void run() {
        log.info("Color = " + this.color.toString()
                + ", ballSize = " + this.ballSize
                + ", speed = " + (this.deltaX/deltaY) * 0.05
        );
        try {
            while (true) {
                move();
                Thread.sleep(50);
            }

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
