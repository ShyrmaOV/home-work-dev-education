package org.bitbucket.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class TokenProvider {

    private static final Logger LOGGER = Logger.getLogger(TokenProvider.class.getName());

    private String secretKey;

    private long tokenValidity;

    private long tokenValidityForRememberMe;

    private final Properties properties = new Properties();

    public void init() {
        try {
            properties.load(new FileReader(new File("token.properties")));
        } catch (IOException e) {
            LOGGER.warn("Enter {}", e);
        }
        this.secretKey = properties.getProperty("SECRET_KEY");
        this.tokenValidity = TimeUnit.HOURS.toMillis(
                Long.parseLong(String.valueOf(properties.get("TOKEN_VALIDITY")))
        );
        this.tokenValidityForRememberMe = TimeUnit.SECONDS.toMillis(
                Long.parseLong(String.valueOf(properties.get("REMEMBER_ME_VALIDITY_SECONDS")))
        );
    }

    public String generateToken(String username, Set<String> authorities, Boolean rememberMe) {
        long now = (new Date()).getTime();
        long validity = rememberMe ? tokenValidityForRememberMe : tokenValidity;

        return Jwts.builder()
                .setSubject(username)
                .claim(String.valueOf(properties.get("AUTHORITIES_KEY")), String.join(",", authorities))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(new Date(now + validity))
                .compact();
    }

    public JWTCredential getCredential(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();

        Set<String> authorities
                = Arrays.stream(claims.get(properties.getProperty("AUTHORITIES_KEY")).toString().split(","))
                .collect(Collectors.toSet());

        return new JWTCredential(claims.getSubject(), authorities);
    }

    public boolean isValidToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            LOGGER.log(Level.INFO, "Invalid JWT signature: {0}", e);
            return false;
        }
    }
}
