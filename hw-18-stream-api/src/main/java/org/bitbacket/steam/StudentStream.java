package org.bitbacket.steam;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.bitbacket.steam.ListOfStudents.students;

public class StudentStream {

    public static List<Student> getFaculty(String faculty){
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public static List<Student> facultyAndCourse(String faculty, String course){
        if (faculty == null || course == null || faculty == "" || course == "") {
            return null;
        }
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .filter(f -> f.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public static List<Student> afterYear(int year){
        if (year <= 0) {
            return null;
        }
        return students.stream()
                .filter(f -> f.getYearOfBirth() > year)
                .collect(Collectors.toList());
    }

    public static List<Student> firstYear(int year){
        if (year <= 0) {
            return null;
        }
        return students.stream()
                .filter(f -> f.getYearOfBirth() > year)
                .findFirst().stream()
                .collect(Collectors.toList());
    }

    public static List<String> listGroup(String group){
        if(group == null || group == ""){
            return null;
        }
        return students.stream()
                .filter(f -> f.getGroup().equals(group))
                .flatMap(student -> Stream.of(student.getFirstName() + " " + student.getLastName()))
                .collect(Collectors.toList());
    }

    public static long studentCount (String faculty){
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .count();
    }

    public static List<Student> setStudentsFaculty (String faculty){
        if(faculty == null || faculty == ""){
            return null;
        }
        return students.stream()
                .peek(s -> {
                    if (s.getFaculty().equals(s.getFaculty())) {
                        s.setFaculty(faculty);
                    }
                })
                .collect(Collectors.toList());
    }

    public static List<Student> setStudentsGroup (String group){
        if(group == null || group == ""){
            return null;
        }
        return students.stream()
                .peek(s -> {
                    if (s.getGroup().equals(s.getGroup())) {
                        s.setGroup(group);
                    }
                })
                .collect(Collectors.toList());
    }

    public static long studentFind(String faculty) {
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .findAny()
                .stream().count();
    }

    public static String methodReduce() {
        return students.stream()
                .flatMap(s -> Stream.of(s.getFirstName() + " " + s.getLastName() + " - "
                        + s.getFaculty() + " " + s.getGroup() + "\n"))
                .reduce("", (partial, elem) -> partial + elem);
    }

    public static Map<String, List<Student>> groupFaculty() {
        Map<String, List<Student>> resultMap = students.stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
        return resultMap;
    }

    public static Map<String, List<Student>> groupCourse() {
        Map<String, List<Student>> mapCourse = students.stream()
                .collect(Collectors.groupingBy(Student::getCourse));
        return mapCourse;
    }

    public static Map<String, List<Student>> groupGroup() {
        Map<String, List<Student>> mapGroup = students.stream()
                .collect(Collectors.groupingBy(Student::getGroup));
        return mapGroup;
    }

    public static boolean allStudentsFaculty(String faculty) {
        return students.stream()
                .allMatch(f -> f.getFaculty().equals(faculty));
    }

    public static boolean oneStudentFaculty(String faculty) {
        return students.stream()
                .anyMatch(f -> f.getFaculty().equals(faculty));
    }

    public static boolean allStudentsGroup(String group) {
        return students.stream()
                .allMatch(g -> g.getGroup().equals(group));
    }

    public static boolean oneStudentGroup(String group) {
        return students.stream()
                .anyMatch(g -> g.getGroup().equals(group));
    }
}
