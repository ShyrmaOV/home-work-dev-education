package org.bitbacket.steam;

import java.util.ArrayList;
import java.util.List;

public class ListOfStudents {

    public static List<Student> students = new ArrayList<>();

    public static List<Student> getStudentsList() {
        students.clear();
        students.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        students.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        students.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "IMDM-52"));
        students.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        students.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "IMD-31"));
        students.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "SDM-62"));
        return students;
    }
}
