package org.bitbacket.steam;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.bitbacket.steam.StudentStream.*;
import static org.junit.Assert.*;

public class StudentStreamTest {

    @Before
    public void setUp(){
        ListOfStudents.getStudentsList();
    }

    /*============================================================================*/
    /*=========================== getFaculty =====================================*/
    /*============================================================================*/

    @Test
    public void getFacultyMany (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        exp.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        List<Student> act = getFaculty("Telecom");
        assertEquals(exp, act);
    }

    @Test
    public void getFacultyOne (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "SDM-62"));
        List<Student> act = getFaculty("Security");
        assertEquals(exp, act);
    }

    @Test
    public void getFacultyNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = getFaculty(null);
        assertEquals(exp, act);
    }

    @Test
    public void getFacultyEmpty() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = getFaculty("");
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*======================== facultyAndCourse ==================================*/
    /*============================================================================*/

    @Test
    public void facultyAndCourseMany (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        exp.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        List<Student> act = facultyAndCourse("Telecom", "4");
        assertEquals(exp, act);
    }

    @Test
    public void facultyAndCourseOne (){
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        List<Student> act = facultyAndCourse("Telecom", "1");
        assertEquals(exp, act);
    }

    @Test
    public void facultyAndCourseNull() {
        assertNull(facultyAndCourse(null, "1"));
    }

    @Test
    public void facultyAndCourseEmpty() {
        List<Student> act = facultyAndCourse("", "2");
        assertNull(act);
    }

    @Test
    public void facultyAndCourseNullTwo() {
        assertNull(facultyAndCourse("5 faculty", null));
    }

    @Test
    public void facultyAndCourseEmptyTwo() {
        List<Student> act = facultyAndCourse("5 faculty", "");
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ afterYear =====================================*/
    /*============================================================================*/

    @Test
    public void afterYearMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        exp.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        exp.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "IMDM-52"));
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        exp.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "IMD-31"));
        List<Student> act = afterYear(1995);
        assertEquals(exp, act);
    }

    @Test
    public void afterYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        List<Student> act = afterYear(2000);
        assertEquals(exp, act);
    }

    @Test
    public void afterYearEmpty() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = afterYear(2005);
        assertEquals(exp, act);
    }


    @Test
    public void afterYearOdd() {
        List<Student> act = afterYear(-2);
        assertNull(act);
    }

    @Test
    public void afterYearZero() {
        List<Student> act = afterYear(0);
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ firstYear =====================================*/
    /*============================================================================*/

    @Test
    public void firstYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        List<Student> act = firstYear(1997);
        assertEquals(exp, act);
    }

    @Test
    public void firstYearTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        List<Student> act = firstYear(2001);
        assertEquals(exp, act);
    }

    @Test
    public void firstYearOdd() {
        List<Student> act = firstYear(-2);
        assertNull(act);
    }

    @Test
    public void firstYearZero() {
        List<Student> act = firstYear(0);
        assertNull(act);
    }

    /*============================================================================*/
    /*============================ listGroup =====================================*/
    /*============================================================================*/

    @Test
    public void listGroupMany() {
        List<String> exp = new ArrayList<>();
        exp.add("Alex Shirma");
        exp.add("Max Korzh");
        List<String> act = listGroup("TSD-41");
        assertEquals(exp, act);
    }

    @Test
    public void listGroupOne() {
        List<String> exp = new ArrayList<>();
        exp.add("Maria Rost");
        List<String> act = listGroup("TSD-14");
        assertEquals(exp, act);
    }

    @Test
    public void listGroupNull() {
        assertNull(listGroup(null));
    }

    @Test
    public void listGroupEmpty() {
        List<String> act = listGroup("");
        assertNull(act);
    }

    /*============================================================================*/
    /*=========================== studentCount ===================================*/
    /*============================================================================*/

    @Test
    public void studentCountMany() {
        long exp = 2;
        long act = studentCount("IT");
        assertEquals(exp, act);
    }

    @Test
    public void studentCountOne() {
        long exp = 1;
        long act = studentCount("Security");
        assertEquals(exp, act);
    }


    @Test
    public void studentCountZero() {
        long exp = 0;
        long act = studentCount(null);
        assertEquals(exp, act);
    }

    @Test
    public void studentCountEmpty() {
        long exp = 0;
        long act = studentCount("");
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*=========================== setStudentsFaculty =============================*/
    /*============================================================================*/

    @Test
    public void setStudentsFacultyMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        exp.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        exp.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "Telecom", "5", "IMDM-52"));
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));
        exp.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "Telecom", "3", "IMD-31"));
        exp.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Telecom", "3", "SDM-62"));
        List<Student> act = setStudentsFaculty("Telecom");
        assertEquals(exp, act);
    }

    @Test
    public void setStudentsFacultyNull() {
        assertNull(setStudentsFaculty(null));
    }

    @Test
    public void setStudentsFacultyEmpty() {
        List<Student> act = setStudentsFaculty("");
        assertNull(act);
    }

    /*============================================================================*/
    /*============================= setStudentsGroup =============================*/
    /*============================================================================*/

    @Test
    public void setStudentsGroupMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSDM-51"));
        exp.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSDM-51"));
        exp.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "TSDM-51"));
        exp.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSDM-51"));
        exp.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "TSDM-51"));
        exp.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "TSDM-51"));
        List<Student> act = setStudentsGroup("TSDM-51");
        assertEquals(exp, act);
    }

    @Test
    public void setStudentsGroupNull() {
        assertNull(setStudentsGroup(null));
    }

    @Test
    public void setStudentsGroupEmpty() {
        List<Student> act = setStudentsGroup("");
        assertNull(act);
    }

    /*============================================================================*/
    /*================================ studentFind ===============================*/
    /*============================================================================*/

    @Test
    public void studentFindMany() {
        long exp = 1;
        long act = studentFind("IT");
        assertEquals(exp, act);
    }

    @Test
    public void studentFindOne() {
        long exp = 1;
        long act = studentFind("Security");
        assertEquals(exp, act);
    }


    @Test
    public void studentFindZero() {
        long exp = 0;
        long act = studentFind(null);
        assertEquals(exp, act);
    }

    @Test
    public void studentFindEmpty() {
        long exp = 0;
        long act = studentFind("");
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================ methodReduce ==============================*/
    /*============================================================================*/

    @Test
    public void methodReduceOne() {
        String exp =
                "Alex Shirma - Telecom TSD-41\n" +
                "Max Korzh - Telecom TSD-41\n" +
                "Dima Ferson - IT IMDM-52\n" +
                "Maria Rost - Telecom TSD-14\n" +
                "Valeri Shirma - IT IMD-31\n" +
                "Alex Dodul - Security SDM-62\n";
        String act = methodReduce();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================ groupFaculty ==============================*/
    /*============================================================================*/

    @Test
    public void sortFacultyTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> telecom = new ArrayList<>();
        telecom.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        telecom.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));
        telecom.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));

        List<Student> it = new ArrayList<>();
        it.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "IMDM-52"));
        it.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "IMD-31"));

        List<Student> security = new ArrayList<>();
        security.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "SDM-62"));

        exp.put("Telecom", telecom);
        exp.put("IT", it);
        exp.put("Security", security);
        Map<String, List<Student>> act = groupFaculty();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================ groupCourse ===============================*/
    /*============================================================================*/

    @Test
    public void groupCourseTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> course1 = new ArrayList<>();
        course1.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));

        List<Student> course3 = new ArrayList<>();
        course3.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "IMD-31"));
        course3.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "SDM-62"));

        List<Student> course4 = new ArrayList<>();
        course4.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        course4.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));

        List<Student> course5 = new ArrayList<>();
        course5.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "IMDM-52"));

        exp.put("1", course1);
        exp.put("3", course3);
        exp.put("4", course4);
        exp.put("5", course5);
        Map<String, List<Student>> act = groupCourse();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*================================= groupGroup ===============================*/
    /*============================================================================*/

    @Test
    public void groupGroupTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> tsd41 = new ArrayList<>();
        tsd41.add(new Student(1, "Alex", "Shirma", 1998, "Kiev", "+380980617334", "Telecom", "4", "TSD-41"));
        tsd41.add(new Student(2, "Max", "Korzh", 1997, "Kiev", "+380967844866", "Telecom", "4", "TSD-41"));

        List<Student> imdm52 = new ArrayList<>();
        imdm52.add(new Student(3, "Dima", "Ferson", 1996, "Kherson", "+380670617834", "IT", "5", "IMDM-52"));

        List<Student> tsd14 = new ArrayList<>();
        tsd14.add(new Student(4, "Maria", "Rost", 2002, "Kiev", "+380665677334", "Telecom", "1", "TSD-14"));

        List<Student> imd31 = new ArrayList<>();
        imd31.add(new Student(5, "Valeri", "Shirma", 2000, "Odessa", "+380660617765", "IT", "3", "IMD-31"));

        List<Student> sdm62 = new ArrayList<>();
        sdm62.add(new Student(6, "Alex", "Dodul", 1995, "Mykolaiv", "+380970567894", "Security", "3", "SDM-62"));

        exp.put("TSD-14", tsd14);
        exp.put("IMD-31", imd31);
        exp.put("TSD-41", tsd41);
        exp.put("IMDM-52", imdm52);
        exp.put("SDM-62", sdm62);
        Map<String, List<Student>> act = groupGroup();
        assertEquals(exp, act);
    }

    /*============================================================================*/
    /*============================= allStudentsFaculty ===========================*/
    /*============================================================================*/

    @Test
    public void allStudentsFacultyFirst() {
        boolean act = allStudentsFaculty("2 faculty");
        assertFalse(act);
    }

    @Test
    public void allStudentsFacultyNull() {
        boolean act = allStudentsFaculty(null);
        assertFalse(act);
    }

    @Test
    public void allStudentsFacultyEmpty() {
        boolean act = allStudentsFaculty("");
        assertFalse(act);
    }

    /*============================================================================*/
    /*============================= oneStudentFaculty ============================*/
    /*============================================================================*/

    @Test
    public void oneStudentFacultyFirst() {
        boolean act = oneStudentFaculty("2 faculty");
        assertFalse(act);
    }

    @Test
    public void oneStudentFacultyNull() {
        boolean act = oneStudentFaculty(null);
        assertFalse(act);
    }

    @Test
    public void oneStudentFacultyEmpty() {
        boolean act = oneStudentFaculty("");
        assertFalse(act);
    }

    /*============================================================================*/
    /*============================= allStudentsGroup ============================*/
    /*============================================================================*/

    @Test
    public void allStudentsGroupFirst() {
        boolean act = allStudentsGroup("530");
        assertFalse(act);
    }

    @Test
    public void allStudentsGroupNull() {
        boolean act = allStudentsGroup(null);
        assertFalse(act);
    }

    @Test
    public void allStudentsGroupEmpty() {
        boolean act = allStudentsGroup("");
        assertFalse(act);
    }

    /*============================================================================*/
    /*============================== oneStudentGroup =============================*/
    /*============================================================================*/

    @Test
    public void oneStudentGroupFirst() {
        boolean act = oneStudentGroup("530");
        assertFalse(act);
    }

    @Test
    public void oneStudentGroupNull() {
        boolean act = oneStudentGroup(null);
        assertFalse(act);
    }

    @Test
    public void oneStudentGroupEmpty() {
        boolean act = oneStudentGroup("");
        assertFalse(act);

    }
}