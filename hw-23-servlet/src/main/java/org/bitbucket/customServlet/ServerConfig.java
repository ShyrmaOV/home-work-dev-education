package org.bitbucket.customServlet;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

public class ServerConfig {
    public static void start() {
        Tomcat tomcat = new Tomcat();
        String webPort = System.getenv("PORT");
        if(webPort == null || webPort.isEmpty()){
            webPort = "8082";
        }
        tomcat.setPort(Integer.parseInt(webPort));
        try {
            File file = new File("hw-23-servlet/web");
            Context ctx = tomcat.addWebapp("", file.getAbsolutePath());
            tomcat.addServlet("", "CustomServlet", new CustomServlet());
            ctx.addServletMappingDecoded("/servlet", "CustomServlet");
            tomcat.start();

        } catch (ServletException | LifecycleException e) {
            e.printStackTrace();
        }
        tomcat.getServer().await();
    }
}
