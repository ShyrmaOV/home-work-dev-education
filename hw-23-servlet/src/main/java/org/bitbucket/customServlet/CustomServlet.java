package org.bitbucket.customServlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/servlet")
public class CustomServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getMethod().equals("POST")) {
            doPost(req, resp);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Enter doPost LoginServlet");
        response.setContentType("text/html");

        String username = request.getParameter("log");
        String password = request.getParameter("pass");
        System.out.println(username + " " + password);
        String destPage = "";
        if (username.equals("user") && password.equals("password")) {
            HttpSession session = request.getSession();
            session.setAttribute("user", username);
            destPage = "profile.html";
        } else {
            response.setStatus(401);
            destPage = "index.jsp";
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(destPage);
        dispatcher.forward(request, response);
    }
}
