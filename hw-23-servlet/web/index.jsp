<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Authorization</title>
    <link rel="stylesheet" href="styles.css" type="text/css">
</head>
<body>
<main>
<section>
    <article>
        <h1 >Login</h1>
        <form action="${pageContext.request.contextPath}/servlet" method="post" novalidate>
            <div class="authorize">
                <div class="login">
                    <input type="text" name="log" placeholder="Login">
                </div>
                <div class="password">
                    <input type="password" name="pass" placeholder="Password">
                </div>
                <div class="enter">
                    <button >Submit</button>
                </div>
            </div>

        </form>

    </article>
</section>
</main>
</body>
</html>