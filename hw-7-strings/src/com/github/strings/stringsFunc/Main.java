package com.github.strings.stringsFunc;

public class Main {
    public static void main(String[] args) {
        String phrase = "Hello world. Its me,Alex.";
        System.out.println(Main.minWord(phrase));
        System.out.println(Main.dollar(phrase, 5));
        System.out.println(Main.replace(phrase));
//        System.out.println(Main.sameSymbol(phrase));
        System.out.println(Main.amount(Main.replace(phrase)));
        System.out.println(Main.delete(phrase, 5, 6));
        System.out.println(Main.revers(phrase));
        System.out.println(Main.deleteLastWord(Main.replace(phrase)));

    }
    public static int minWord (String phrase) {
        String[] words = phrase.replaceAll("[.,]", "").split(" ");
        int min = words[0].length();
        for (int i = 0; i < words.length; i++) {
            if (words[i].length() < min) {
                min = words[i].length();
            }
        }
        return min;
    }
    public static String dollar(String phrase, int length) {
        String[] words = phrase.replaceAll("[.,]", "").split(" ");
        String result = "";
        for (int i = 0; i < words.length; i++) {
            if(words[i].length() == length) {
                words[i] = words[i].substring(0, length - 3);
                words[i] = words[i].concat("$$$ ");
                result += words[i];
//                System.out.print(words[i]);
            }
            else {
                result += words[i] + " ";
//                System.out.print(words[i] + " ");
            }
        }
        return result;
    }
    public static String replace(String phrase) {
        phrase = phrase.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        return phrase;
    }
//    public static String sameSymbol(String phrase) {
//        String[] symbol = phrase.split("");
//        String[] regex = new String[symbol.length];
//        for (int i = 0; i < symbol.length - 1; i++) {
//            for (int j = i + 1; j < symbol.length; j++) {
//                if (symbol[i].equals(symbol[j])) {
//                    regex[i] = symbol[i];
////                    System.out.print(regex);
//                    break;
//                } else
//                    regex[i] = null;
//            }
//        }
//        for (int i = 0; i < regex.length; i++) {
//            if (regex[i].equals(null)){
//break;
//            } else {
//                phrase = phrase.replaceAll(regex[i], "");
//            }
//        }
//        return phrase;
//    }
    public static int amount (String phrase) {
        String[] words = phrase.split(" ");
        return words.length;
    }
    public static String delete(String phrase, int start, int length) {

        String deleted = phrase.substring(start, start + length);

        return phrase.replace(deleted, "");
    }
    public static String revers (String phrase) {
        String[] words = phrase.split("");
        String revers = "";
        for (int i = words.length - 1; i >= 0; i--) {
            revers += words[i];
        }
        return revers;
    }
    public static String deleteLastWord(String phrase) {
        String[] words = phrase.split(" ");
        String result = "";
        for (int i = 0; i < words.length - 1; i++) {
            result += words[i] + " ";
        }

        return result;
    }
}
