package com.github.strings.stringsFunc;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {
    public String data = "Hello world. Its me,Alex.";
    @Test
    public void minWord() {
        int exp = 3;
        int act = Main.minWord(data);
        assertEquals(exp, act);
    }

    @Test
    public void dollar() {
        String exp = "He$$$ wo$$$ Its meAlex ";
        String act = Main.dollar(data, 5);
        assertEquals(exp, act);
    }

    @Test
    public void replace() {
        String exp = "Hello world. Its me, Alex.";
        String act = Main.replace(data);
        assertEquals(exp, act);
    }

    @Test
    public void amount() {
        int exp = 4;
        int act = Main.amount(data);
        assertEquals(exp, act);
    }

    @Test
    public void delete() {
        String exp = "Hello. Its me,Alex.";
        String act = Main.delete(data, 5, 6);
        assertEquals(exp, act);
    }

    @Test
    public void revers() {
        String exp = ".xelA,em stI .dlrow olleH";
        String act = Main.revers(data);
        assertEquals(exp, act);
    }

    @Test
    public void deleteLastWord() {
        String exp = "Hello world. Its ";
        String act = Main.deleteLastWord(data);
        assertEquals(exp, act);
    }
}