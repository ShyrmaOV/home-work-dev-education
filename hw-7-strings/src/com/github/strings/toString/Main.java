package com.github.strings.toString;

public class Main {
    public static void main(String[] args) {
        String textInt = Main.toString(5);
        String textDouble = Main.toString(5.0);
        int numInt = Main.fromStringToInt("5");
        double numDouble = Main.fromStringToDouble("5");
        System.out.println(textInt);
        System.out.println(textDouble);
        System.out.println(numInt);
        System.out.println(numDouble);

    }
    public static String toString(int arg) {
        return "" + arg;
    }
    public static String toString(double arg) {
        return "" + arg;
    }
    public static int fromStringToInt(String arg) {
        return Integer.valueOf(arg);
    }
    public static double fromStringToDouble(String arg) {
        return Double.valueOf(arg);
    }
}
