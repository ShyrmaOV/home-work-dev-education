package com.github.strings.toString;

import org.junit.Test;
import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void testToString() {
        String exp = "5";
        int data = 5;
        String act = Main.toString(data);
        assertEquals(exp, act);
    }

    @Test
    public void testToString1() {
        String exp = "5.0";
        double data = 5.0;
        String act = Main.toString(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringToInt() {
        int exp = 5;
        String data = "5";
        int act = Main.fromStringToInt(data);
        assertEquals(exp, act);
    }

    @Test
    public void fromStringToDouble() {
        double exp = 5;
        String data = "5.0";
        double act = Main.fromStringToDouble(data);
        assertEquals(exp, act, 0.001);
    }
}