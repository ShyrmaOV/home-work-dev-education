package com.github.strings.symbol;

public class Main {
    public static void main(String[] args) {
        Main.symbol('a', 'z');
        Main.symbol('A', 'Z');
        Main.symbol('а', 'я');
        Main.allSymbol();

    }

    public static void symbol(char a, char b) {
        for (char i = a; i <= b; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void allSymbol() {
        for (char i = 0; i <= 255; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
