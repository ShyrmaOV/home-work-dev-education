package org.bitbucket.customHttpServer;

public class HttpServerRun {

    public static void main(String[] args) {
        HttpHandler socketHandler = new HttpHandler(8040);
        Thread socketThread = new Thread(socketHandler, "Server");
        socketThread.start();
        System.out.println(socketThread.getName() + " run.");
    }
}
