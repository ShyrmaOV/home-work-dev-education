package org.bitbucket.customHttpServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class HttpHandler implements Runnable{

    private final boolean isRun = true;

    private final int port;

    private ServerSocket serverSocket;

    Map<String, Integer> doMethod = new HashMap<>();

    public HttpHandler(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            try {
                throw new IOException("Failed to open port " + port);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        while (this.isRun) {
            try {
                Socket socket = serverSocket.accept();
                SimpleHttpRequest req = new SimpleHttpRequest();
                SimpleHttpResponse resp = new SimpleHttpResponse();
                req.takeRequest(socket);
                this.executeMethod(req, resp);
                resp.sendResponse(socket);
            } catch (IOException ignore) {
            }
        }
    }

    private void executeMethod(SimpleHttpRequest req, SimpleHttpResponse resp) throws IOException {
        doMethod.put("GET", this.doGet(req, resp));
        doMethod.put("POST", this.doPost(req, resp));
        doMethod.put("DELETE", this.doDelete(req, resp));

        if (doMethod.containsKey(req.getMethod())) {
        }
//        switch (req.getMethod()) {
//            case "GET":
//                this.doGet(req, resp);
//                break;
//            case "HEAD":
//                this.doHead(req, resp);
//                break;
//            case "POST":
//                this.doPost(req, resp);
//                break;
//            case "PUT":
//                this.doPut(req, resp);
//                break;
//            case "DELETE":
//                this.doDelete(req, resp);
//                break;
//            case "CONNECT":
//                this.doConnect(req, resp);
//                break;
//            case "OPTIONS":
//                this.doOptions(req, resp);
//                break;
//            case "TRACE":
//                this.doTrace(req, resp);
//                break;
//            case "PATCH":
//                this.doPatch(req, resp);
//                break;
//            default:
//                throw new IOException("Method " + req.getMethod() + " is allowed.");
//        }
    }

    public int doGet(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doGet</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
        return 1;
    }

    public int doPost(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPost</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
        return 2;
    }

    public void doHead(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doHead</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
    }

    public void doPut(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPut</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public int doDelete(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doDelete</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
        return 3;
    }

    public void doConnect(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doConnect</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doOptions(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doOptions</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doTrace(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doTrace</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doPatch(SimpleHttpRequest req, SimpleHttpResponse resp) {
        String outString = "<h1>doPatch</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeader("Content-Length", "" + outString.length());
        resp.addHeader("Content-Type", "text/html");
        resp.setBody(outString);
    }
}
