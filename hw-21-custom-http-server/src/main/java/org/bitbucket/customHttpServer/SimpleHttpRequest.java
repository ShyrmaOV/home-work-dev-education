package org.bitbucket.customHttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SimpleHttpRequest {

    private String method;

    private String protocol;

    private Map<String, String> headers;

    private String body;

    public SimpleHttpRequest() {
    }

    public void takeRequest(Socket socket) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String buffer;
        String[] arrayBuffer;
        buffer = reader.readLine();
        if (buffer == null) {
            throw new IOException("Failed to read request");
        }
        arrayBuffer = buffer.split("/");
        this.method = arrayBuffer[0].trim();
        this.protocol = arrayBuffer[1].trim();
        this.headers = readHeaders(reader);
        this.body = readBody(reader, this.headers);
    }

    private Map<String, String> readHeaders(BufferedReader reader) throws IOException {
        String buffer;
        String[] arrayBuffer;
        Map<String, String> result = new HashMap<>();
        while (Objects.nonNull((buffer = reader.readLine())) && !buffer.isEmpty()) {
            arrayBuffer = buffer.split(":");
            result.put(arrayBuffer[0].trim(), arrayBuffer[1].trim());
        }
        return result;
    }

    private String readBody(BufferedReader reader, Map<String, String> headers) throws IOException {
        if (headers.containsKey("Content-Length")) {
            int length = Integer.parseInt(headers.get("Content-Length"));
            char[] result = new char[length];
            reader.read(result, 0, length);
            return new String(result);
        }
        return "";
    }

    public String getMethod() {
        return method;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.method).append(" / ").append(this.protocol).append("\n\r");
        for (String header : this.headers.keySet()) {
            builder.append(header).append(": ").append(this.headers.get(header)).append("\n\r");
        }
        builder.append("\n\r");
        builder.append(this.body).append("\n\r\n\r");
        return builder.toString();
    }
}
