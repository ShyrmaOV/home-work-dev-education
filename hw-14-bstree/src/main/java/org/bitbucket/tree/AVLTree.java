package org.bitbucket.tree;

import java.util.Objects;

public class AVLTree implements ITree {

    private Node root;

    private static class Node {

        int val, height;

        Node left, right;

        public Node(int val) {
            this.val = val;
            this.height = 1;
            this.left = this.right = null;
        }
    }

    public AVLTree() {
        this.root = null;
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int i : init) {
            add(i);
        }
    }

    @Override
    public void print() {

    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (Objects.isNull(p)) {
            return 0;
        }
        return (sizeNode(p.left) + 1 + sizeNode(p.right));
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.val;
        toArrayNode(array, c, node.right);
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
        } else {
            addNode(this.root, val);
        }
    }

    private void addNode(Node p, int val) {
        if (p.val > val) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(p.left, val);
            }
        } else if (p.val < val) {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(p.right, val);
            }
        } else {
            throw new RuntimeException("duplicate Key!");
        }
//        reBalance(p);
    }

    public int leaf() {
        return leavesNode(this.root);
    }

    private int leavesNode(Node p) {
        if (Objects.isNull(p)) {
            return 0;
        }
        if (Objects.isNull(p.left) && Objects.isNull(p.right)) {
            return 1;
        } else {
            return (leavesNode(p.left) + leavesNode(p.right));
        }
    }

    public int nodes() {
        return nodeCounter(this.root);
    }

    private int nodeCounter(Node p) {
        if (Objects.isNull(p) || (Objects.isNull(p.right) && Objects.isNull(p.left))) {
            return 0;
        }
        return 1 + nodeCounter(p.left) + nodeCounter(p.right);
    }

    public int height() {
        return heightNode(this.root);
    }

    public int heightNode(Node p) {
        return p == null ? -1 : p.height;
    }

    public void updateHeight(Node p) {
        p.height = 1 + Math.max(heightNode(p.left), heightNode(p.right));
    }

    public int getBalance(Node n) {
        return (n == null) ? 0 : heightNode(n.right) - heightNode(n.left);
    }

    Node rotateRight(Node p) {
        Node x = p.left;
        Node z = x.right;
        x.right = p;
        p.left = z;
        updateHeight(p);
        updateHeight(x);
        return x;
    }

    Node rotateLeft(Node p) {
        Node x = p.right;
        Node z = x.left;
        x.left = p;
        p.right = z;
        updateHeight(p);
        updateHeight(x);
        return x;
    }

    Node reBalance(Node p) {
        updateHeight(p);
        int balance = getBalance(p);
        if (balance > 1) {
            if (heightNode(p.right.right) > heightNode(p.right.left)) {
                p = rotateLeft(p);
            }
            p.right = rotateRight(p.right);
            p = rotateLeft(p);

        } else if (balance < -1) {
            if (heightNode(p.left.left) <= heightNode(p.left.right)) {
                p.left = rotateLeft(p.left);
            }
            p = rotateRight(p);
        }
        return p;
    }

    public int width() {
        return widthNode(this.root);
    }

    private int widthNode(Node p) {
        if (Objects.isNull(p)) {
            return -1;
        }
        return (Math.max(heightNode(p.left), heightNode(p.right)) + 1);

    }

    public void reverse() {
        if (Objects.isNull(this.root)) {
            throw new NullPointerException();
        } else {
            reverseNode(this.root);
        }
    }

    private void reverseNode(Node p) {
        final Node temp = p.right;
        p.right = p.left;
        p.left = temp;

        if (p.left != null) {
            reverseNode(p.left);
        }

        if (p.right != null) {
            reverseNode(p.right);
        }
    }

    @Override
    public void del(int val) {
        delNode(this.root, val);
    }

    private Node delNode(Node p, int val) {
        if (p == null) {
            return null;
        } else if (p.val > val) {
            p.left = delNode(p.left, val);
        } else if (p.val < val) {
            p.right = delNode(p.right, val);
        } else {
            if (p.left == null || p.right == null) {
                p = (p.left == null) ? p.right : p.left;
            } else {
                Node mostLeftChild = mostLeftChild(p.right);
                p.val = mostLeftChild.val;
                p.right = delNode(p.right, p.val);
            }
        }
        if (p != null) {
            p = reBalance(p);
        }
        return p;
    }

    private Node mostLeftChild(Node p) {
        Node tmp = p;

        if (Objects.nonNull(tmp.left)) {
            tmp = mostLeftChild(tmp.left);
        }
        return tmp;
    }

    private static class Counter {
        int index = 0;
    }
}
