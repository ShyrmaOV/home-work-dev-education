package org.bitbucket.tree;

import java.util.Objects;

public class BSTree implements ITree {

    private Node root;

    private static class Node {

        int value;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
            this.left = this.right = null;
        }
    }

    public BSTree() {
        this.root = null;
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int i : init) {
            add(i);
        }
    }

    @Override
    public void print() {
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        return (sizeNode(p.left) + 1 + sizeNode(p.right));
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
        } else {
            addNode(val, this.root);
        }
    }

    private void addNode(int val, Node p) {
        if (val < p.value) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(val, p.left);
            }
        } else if (val > p.value) {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(val, p.right);
            }
        } else {
            throw new RuntimeException("duplicate Key!");
        }
    }

    public int leaf() {
        return leavesNode(this.root);
    }

    private int leavesNode(Node p) {
        if (Objects.isNull(p)) {
            return 0;
        }
        if (Objects.isNull(p.left) && Objects.isNull(p.right)) {
            return 1;
        } else {
            return (leavesNode(p.left) + leavesNode(p.right));
        }
    }

    public int nodes() {
        return nodeCounter(this.root);
    }

    private int nodeCounter(Node p) {
        if (Objects.isNull(p) || (Objects.isNull(p.right) && Objects.isNull(p.left))) {
            return 0;
        }
        return 1 + nodeCounter(p.left) + nodeCounter(p.right);
    }

    public int height() {
        return heightNode(this.root);
    }

    private int heightNode(Node p) {
        if (Objects.isNull(p)) {
            return -1;
        }
        return (Math.max(heightNode(p.left), heightNode(p.right)) + 1);
    }

    public int width() {
        return widthNode(this.root);
    }

    private int widthNode(Node p) {
        if (Objects.isNull(p)) {
            return -1;
        }
        return (Math.max(heightNode(p.left), heightNode(p.right)) + 1);

    }

    public void reverse() {
        if (Objects.isNull(this.root)) {
            throw new NullPointerException();
        } else {
            reverseNode(this.root);
        }
    }

    private void reverseNode(Node p) {
        final Node temp = p.right;
        p.right = p.left;
        p.left = temp;

        if (p.left != null) {
            reverseNode(p.left);
        }

        if (p.right != null) {
            reverseNode(p.right);
        }
    }

    public void del(int val) {
        if (Objects.isNull(this.root)) {
            throw new NullPointerException();
        } else {
            this.root = delNode(this.root, val);
        }
    }

    private Node delNode(Node p, int val) {
        if (val < p.value) {
            p.left = delNode(p.left, val);
        } else if (val > p.value) {
            p.right = delNode(p.right, val);
        } else {
            if (p.left == null) {
                return p.right;
            } else if (p.right == null) {
                return p.left;
            }
            p.value = minValue(p.right);
            p.right = delNode(p.right, p.value);
        }
        return p;
    }

    private int minValue(Node p) {
        int minVal = p.value;
        while (p.left != null) {
            minVal = p.left.value;
            p = p.left;
        }
        return minVal;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BSTree bsTree = (BSTree) o;
        return Objects.equals(root, bsTree.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root);
    }

    private static class Counter {
        int index = 0;
    }
}
