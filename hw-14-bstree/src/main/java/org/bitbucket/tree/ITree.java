package org.bitbucket.tree;

public interface ITree {

    void init(int[] init);

    void print();

    void clear();

    void add(int val);

    int size();

    int leaf();

    int nodes();

    int height();

    int width();

    void reverse();

    void del(int val);

    int[] toArray();


}
