package org.bitbucket.tree;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class BSTreeTest {

    private final String name;

    private final ITree tree;

    public BSTreeTest(String name, ITree tree) {
        this.name = name;
        this.tree = tree;
    }

    @Before
    public void setUp() {
        this.tree.clear();
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object[]> instances() {
        return Arrays.asList(new Object[][] {
                {"BSTree", new BSTree()},
                {"AVLTree", new AVLTree()}
        });
    }

    //=================================================
    //=================== Init ========================
    //=================================================

    @Test
    public void initNull() {
        this.tree.init(null);

    }

    @Test
    public void initZero() {
        int[] array = new int[0];
        this.tree.init(array);
    }

    @Test
    public void initOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        int[] exp = {1};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initTwo() {
        int[] array = new int[]{1, 2};
        this.tree.init(array);
        int[] exp = {1, 2};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initThree() {
        int[] array = new int[]{3, 4, 2};
        this.tree.clear();
        this.tree.init(array);
        int[] exp = {2, 3, 4};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void initMany() {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 8, 7, 9, 10};
        this.tree.init(array);
        int[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Clean ========================
    //=================================================

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 4};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearThree() {
        int[] array = new int[]{1, 2, 5};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test (expected = RuntimeException.class)
    public void clearMany() {
        int[] array = new int[]{1, 2, 5, 3, 4, 1, 2, 5, 3, 4};
        this.tree.init(array);
        this.tree.clear();
        int[] exp = {};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //====================== Add ======================
    //=================================================

    @Test
    public void addOne() {
        int[] array = new int[0];
        this.tree.init(array);
        this.tree.add(2);
        int[] exp = {2};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addTwo() {
        int[] array = new int[] {3};
        this.tree.init(array);
        this.tree.add(1);
        int[] exp = {1, 3};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addThree() {
        int[] array = new int[] {1, 3};
        this.tree.init(array);
        this.tree.add(2);
        int[] exp = {1, 2, 3};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addMany() {
        int[] array = new int[] {1, 2, 3, 5, 6, 7, 8, 9, 10};
        this.tree.init(array);
        this.tree.add(4);
        int[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test (expected = RuntimeException.class)
    public void addSame() {
        int[] array = new int[] {2, 5, 3, 2, 1, 5, 3, 4};
        this.tree.init(array);
        this.tree.add(1);
        int[] exp = {1, 1, 2, 2, 3, 3, 4, 5, 5};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeZero() {
        int[] array = new int[]{};
        this.tree.init(array);
        int exp = 0;
        int act = this.tree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{3};
        this.tree.init(array);
        int exp = 1;
        int act = this.tree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        int[] array = new int[]{3, 2};
        this.tree.init(array);
        int exp = 2;
        int act = this.tree.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeFive() {
        int[] array = new int[]{1, 2, 3, 4, 5};
        this.tree.init(array);
        int exp = 5;
        int act = this.tree.size();
        assertEquals(exp, act);
    }

    @Test (expected = RuntimeException.class)
    public void sizeMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.tree.init(array);
        int exp = 10;
        int act = this.tree.size();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== Leaf ========================
    //=================================================

    @Test
    public void leaf() {
        int[] array = new int[]{2, 1, 23, 43, 12, 54, 44, 13, 56, 34, 11};
        this.tree.init(array);
        int exp = 6;
        int act = this.tree.leaf();
        assertEquals(exp, act);
    }

    @Test
    public void nodes() {
        int[] array = new int[]{2, 1, 23, 43, 12, 54, 44, 13, 56, 34, 11, 57};
        this.tree.init(array);
        int exp = 6;
        int act = this.tree.nodes();
        assertEquals(exp, act);
    }

    @Test
    public void height() {
        int[] array = new int[]{1, 24, 30, 2, 81, 3, 5, 4, 6, 7};
        this.tree.init(array);
        int exp = 6;
        int act = this.tree.height();
        assertEquals(exp, act);
    }

    @Test
    public void width() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.tree.init(array);
        int exp = 3;
        int act = this.tree.width();
        assertEquals(exp, act);
    }

    @Test
    public void reverse() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.tree.init(array);
        this.tree.reverse();
        int[] exp = new int[] {86, 78, 73, 67, 48, 4};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void del() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.tree.init(array);
        this.tree.del(48);
        int[] exp = new int[] {4, 67, 73, 78, 86};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void del1() {
        int[] array = new int[]{67, 4, 48, 86, 78, 73};
        this.tree.init(array);
        this.tree.del(4);
        int[] exp = new int[] {48, 67, 73, 78, 86};
        int[] act = this.tree.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void toArray() {
    }
}