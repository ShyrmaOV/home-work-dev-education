package com.github.converter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class ChooseConverter extends JFrame{
    JRadioButton jRadioButtonTime;
    JRadioButton jRadioButtonWeight;
    JRadioButton jRadioButtonVolume;
    JRadioButton jRadioButtonLength;
    JRadioButton jRadioButtonTemperature;

    JButton jButton;

    ButtonGroup buttonGroup;

    JLabel chooseCategory;

    public ChooseConverter()
    {
        this.setLayout(null);
        chooseCategory = new JLabel("Choose category:");

        jRadioButtonTime = new JRadioButton();
        jRadioButtonWeight = new JRadioButton();
        jRadioButtonVolume = new JRadioButton();
        jRadioButtonLength = new JRadioButton();
        jRadioButtonTemperature = new JRadioButton();

        jButton = new JButton("Choose");

        buttonGroup = new ButtonGroup();

        jRadioButtonTime.setText("Time");
        jRadioButtonWeight.setText("Weight");
        jRadioButtonVolume.setText("Volume");
        jRadioButtonLength.setText("Length");
        jRadioButtonTemperature.setText("Temperature");

        chooseCategory.setBounds(10, 10, 150, 30);
        jRadioButtonTime.setBounds(120, 10, 120, 30);
        jRadioButtonWeight.setBounds(120, 40, 80, 30);
        jRadioButtonVolume.setBounds(120, 70, 80, 30);
        jRadioButtonLength.setBounds(120, 100, 80, 30);
        jRadioButtonTemperature.setBounds(120, 130, 80, 30);

        // Setting Bounds of "jButton".
        jButton.setBounds(110, 160, 80, 30);

        this.add(jRadioButtonTime);
        this.add(jRadioButtonWeight);
        this.add(jRadioButtonVolume);
        this.add(jRadioButtonLength);
        this.add(jRadioButtonTemperature);
        this.add(jButton);
        this.add(chooseCategory);

        buttonGroup.add(jRadioButtonTime);
        buttonGroup.add(jRadioButtonWeight);
        buttonGroup.add(jRadioButtonVolume);
        buttonGroup.add(jRadioButtonLength);
        buttonGroup.add(jRadioButtonTemperature);

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (jRadioButtonTime.isSelected()) {
                    JFrame frame = new JFrame("Time Converter");
                    frame.setBounds(100, 100, 350, 350);
                    frame.setLayout(null);
                    final JPanel chooseConverter = new TimeConverter().convert();
                    frame.add(chooseConverter);
                    frame.setVisible(true);
                } else if (jRadioButtonWeight.isSelected()) {
                    JFrame frame = new JFrame("Weight Converter");
                    frame.setBounds(100, 100, 350, 350);
                    frame.setLayout(null);
                    final JPanel chooseConverter = new WeightConverter().convert();
                    frame.add(chooseConverter);
                    frame.setVisible(true);
                } else if (jRadioButtonVolume.isSelected()) {
                    JFrame frame = new JFrame("Volume Converter");
                    frame.setBounds(100, 100, 350, 350);
                    frame.setLayout(null);
                    final JPanel chooseConverter = new VolumeConverter().convert();
                    frame.add(chooseConverter);
                    frame.setVisible(true);
                } else if (jRadioButtonLength.isSelected()) {
                    JFrame frame = new JFrame("Length Converter");
                    frame.setBounds(100, 100, 350, 350);
                    frame.setLayout(null);
                    final JPanel chooseConverter = new LengthConverter().convert();
                    frame.add(chooseConverter);
                    frame.setVisible(true);
                } else if (jRadioButtonTemperature.isSelected()) {
                    JFrame frame = new JFrame("Temperature Converter");
                    frame.setBounds(100, 100, 350, 350);
                    frame.setLayout(null);
                    final JPanel chooseConverter = new TemperatureConverter().convert();
                    frame.add(chooseConverter);
                    frame.setVisible(true);
                }
            }
        });
    }
}
