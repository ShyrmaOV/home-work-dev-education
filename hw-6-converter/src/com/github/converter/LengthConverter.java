package com.github.converter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;

public class LengthConverter {
    private JButton myButton;
    private JLabel titleLabel;
    private JLabel centerLabel;
    private JLabel bLabel;
    private JTextField frField;
    private JComboBox<String> fromBox;
    private JTextField toField;
    private JComboBox<String> toBox;
    private final String[] lengthTypes = {"M", "KM", "Miles", "Nautical mile", "Cable", "League", "Foot", "Yards"};
    private final HashMap<String, HashMap<String, Double>> converter = new HashMap<>();

    public JPanel convert() {

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(null);
        firstPanel.setSize( 400, 400);
        firstPanel.setVisible(true);

        titleLabel = new JLabel("Length Converter");
        titleLabel.setBounds(120, 5, 250, 20);
        firstPanel.add(titleLabel);

        frField = new JTextField(20);
        frField.setBounds(20, 35, 300, 30);
        firstPanel.add(frField);

        centerLabel = new JLabel("From:");
        centerLabel.setBounds(20, 85, 100, 30);
        firstPanel.add(centerLabel);

        fromBox = new JComboBox<>(lengthTypes);
        fromBox.setBounds(70, 85, 250, 30);
        firstPanel.add(fromBox);

        bLabel = new JLabel("To:");
        bLabel.setBounds(20, 130, 100, 30);
        firstPanel.add(bLabel);

        toBox = new JComboBox<>(lengthTypes);
        toBox.setBounds(70, 130, 250, 30);
        firstPanel.add(toBox);

        myButton = new JButton("CONVERT");
        myButton.setBounds(20, 180, 300, 50);
        firstPanel.add(myButton);
        myButton.addActionListener(new Action());

        toField = new JTextField(20);
        toField.setBounds(20, 250, 300, 30);
        toField.setEnabled(false);
        firstPanel.add(toField);

        return firstPanel;
    }

    public LengthConverter() {

        converter.put("M", new HashMap<>());
        converter.get("M").put("M", 1.0);
        converter.get("M").put("KM", 0.001);
        converter.get("M").put("Miles", 0.00062);
        converter.get("M").put("Nautical mile", 0.00054);
        converter.get("M").put("Cable", 0.0054);
        converter.get("M").put("League", 0.00021);
        converter.get("M").put("Foot", 3.28084);
        converter.get("M").put("Yards", 1.09361);

        converter.put("KM", new HashMap<>());
        converter.get("KM").put("KM", 1.0);
        converter.get("KM").put("M", 1000.0);
        converter.get("KM").put("Miles", 0.62137);
        converter.get("KM").put("Nautical mile", 0.53996);
        converter.get("KM").put("Cable", 5.39957);
        converter.get("KM").put("League", 0.20712);
        converter.get("KM").put("Foot", 3280.8399);
        converter.get("KM").put("Yards", 1093.6133);

        converter.put("Miles", new HashMap<>());
        converter.get("Miles").put("Miles", 1.0);
        converter.get("Miles").put("M", 1609.34401);
        converter.get("Miles").put("KM", 1.60934);
        converter.get("Miles").put("Nautical mile", 0.86898);
        converter.get("Miles").put("Cable", 8.68976);
        converter.get("Miles").put("League", 0.33333);
        converter.get("Miles").put("Foot", 5280.00003);
        converter.get("Miles").put("Yards", 1760.000009);

        converter.put("Nautical mile", new HashMap<>());
        converter.get("Nautical mile").put("Nautical mile", 1.0);
        converter.get("Nautical mile").put("M", 1852.00001);
        converter.get("Nautical mile").put("KM", 1.852);
        converter.get("Nautical mile").put("Miles", 1.15078);
        converter.get("Nautical mile").put("Cable", 10.0);
        converter.get("Nautical mile").put("League", 0.38359);
        converter.get("Nautical mile").put("Foot", 6076.06563);
        converter.get("Nautical mile").put("Yards", 2025.35521);

        converter.put("Cable", new HashMap<>());
        converter.get("Cable").put("Cable", 1.0);
        converter.get("Cable").put("M", 185.2);
        converter.get("Cable").put("KM", 0.1852);
        converter.get("Cable").put("Miles", 0.11508);
        converter.get("Cable").put("Nautical mile", 0.1);
        converter.get("Cable").put("League", 0.03836);
        converter.get("Cable").put("Foot", 607.6224);
        converter.get("Cable").put("Yards", 202.5408);

        converter.put("League", new HashMap<>());
        converter.get("League").put("League", 1.0);
        converter.get("League").put("M", 4828.03202);
        converter.get("League").put("KM", 4.82803);
        converter.get("League").put("Miles", 3.0);
        converter.get("League").put("Nautical mile", 2.60693);
        converter.get("League").put("Cable", 26.0693);
        converter.get("League").put("Foot", 15840.00787);
        converter.get("League").put("Yards", 5280.00262);

        converter.put("Foot", new HashMap<>());
        converter.get("Foot").put("Foot", 1.0);
        converter.get("Foot").put("M", 0.3048);
        converter.get("Foot").put("KM", 0.0003);
        converter.get("Foot").put("Miles", 0.00019);
        converter.get("Foot").put("Nautical mile", 0.00017);
        converter.get("Foot").put("Cable", 0.00165);
        converter.get("Foot").put("League", 0.00006);
        converter.get("Foot").put("Yards", 0.33333);

        converter.put("Yards", new HashMap<>());
        converter.get("Yards").put("Yards", 1.0);
        converter.get("Yards").put("M", 0.9144);
        converter.get("Yards").put("KM", 0.00091);
        converter.get("Yards").put("Miles", 0.00057);
        converter.get("Yards").put("Nautical mile", 0.00049);
        converter.get("Yards").put("Cable", 0.00494);
        converter.get("Yards").put("League", 0.00019);
        converter.get("Yards").put("Foot", 3.0);
    }
    public class Action implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Double numToConvert = Double.parseDouble(frField.getText());
            Double conversionRate = converter.get(fromBox.getSelectedItem()).get(toBox.getSelectedItem());
            String result = new DecimalFormat("#0.00000").format(numToConvert * conversionRate);
            toField.setText(result);
        }
    }
}

