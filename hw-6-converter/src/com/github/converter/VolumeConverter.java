package com.github.converter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.HashMap;

public class VolumeConverter {
    private JButton myButton;
    private JLabel titleLabel;
    private JLabel centerLabel;
    private JLabel bLabel;
    private JTextField frField;
    private JComboBox<String> fromBox;
    private JTextField toField;
    private JComboBox<String> toBox;
    private final String[] lengthTypes = {"L", "M^3", "Gallon", "Pint", "Quart", "Barrel", "Cubic foot", "Cubic inch"};
    private final HashMap<String, HashMap<String, Double>> converter = new HashMap<>();

    public JPanel convert() {

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(null);
        firstPanel.setSize( 400, 400);

        firstPanel.setVisible(true);

        titleLabel = new JLabel("Volume Converter");
        titleLabel.setBounds(120, 5, 250, 20);
        firstPanel.add(titleLabel);

        frField = new JTextField(20);
        frField.setBounds(20, 35, 300, 30);
        firstPanel.add(frField);

        centerLabel = new JLabel("From:");
        centerLabel.setBounds(20, 85, 100, 30);
        firstPanel.add(centerLabel);

        fromBox = new JComboBox<>(lengthTypes);
        fromBox.setBounds(70, 85, 250, 30);
        firstPanel.add(fromBox);

        bLabel = new JLabel("To:");
        bLabel.setBounds(20, 130, 100, 30);
        firstPanel.add(bLabel);

        toBox = new JComboBox<>(lengthTypes);
        toBox.setBounds(70, 130, 250, 30);
        firstPanel.add(toBox);

        myButton = new JButton("CONVERT");
        myButton.setBounds(20, 180, 300, 50);
        firstPanel.add(myButton);
        myButton.addActionListener(new SubmitListener());

        toField = new JTextField(20);
        toField.setBounds(20, 250, 300, 30);
        toField.setEnabled(false);
        firstPanel.add(toField);

        return firstPanel;
    }

    public VolumeConverter() {

        converter.put("L", new HashMap<>());
        converter.get("L").put("L", 1.0);
        converter.get("L").put("M^3", 0.001);
        converter.get("L").put("Gallon", 0.264);
        converter.get("L").put("Pint", 2.113);
        converter.get("L").put("Quart", 1.057);
        converter.get("L").put("Barrel", 0.006);
        converter.get("L").put("Cubic foot", 0.035);
        converter.get("L").put("Cubic inch", 61.024);

        converter.put("M^3", new HashMap<>());
        converter.get("M^3").put("M^3", 1.0);
        converter.get("M^3").put("L", 1000.0);
        converter.get("M^3").put("Gallon", 264.172);
        converter.get("M^3").put("Pint", 2113.376);
        converter.get("M^3").put("Quart", 1056.688);
        converter.get("M^3").put("Barrel", 6.29);
        converter.get("M^3").put("Cubic foot", 35.316);
        converter.get("M^3").put("Cubic inch", 61026.047);

        converter.put("Gallon", new HashMap<>());
        converter.get("Gallon").put("Gallon", 1.0);
        converter.get("Gallon").put("L", 3.785);
        converter.get("Gallon").put("M^3", 0.004);
        converter.get("Gallon").put("Pint", 8.0);
        converter.get("Gallon").put("Quart", 4.0);
        converter.get("Gallon").put("Barrel", 0.024);
        converter.get("Gallon").put("Cubic foot", 0.134);
        converter.get("Gallon").put("Cubic inch", 231.0);

        converter.put("Pint", new HashMap<>());
        converter.get("Pint").put("Pint", 1.0);
        converter.get("Pint").put("L", 0.473);
        converter.get("Pint").put("M^3", 0.0005);
        converter.get("Pint").put("Gallon", 0.125);
        converter.get("Pint").put("Quart", 0.5);
        converter.get("Pint").put("Barrel", 0.003);
        converter.get("Pint").put("Cubic foot", 0.017);
        converter.get("Pint").put("Cubic inch", 28.875);

        converter.put("Quart", new HashMap<>());
        converter.get("Quart").put("Quart", 1.0);
        converter.get("Quart").put("L", 0.946);
        converter.get("Quart").put("M^3", 0.001);
        converter.get("Quart").put("Gallon", 0.25);
        converter.get("Quart").put("Pint", 2.0);
        converter.get("Quart").put("Barrel", 0.006);
        converter.get("Quart").put("Cubic foot", 0.033);
        converter.get("Quart").put("Cubic inch", 57.75);

        converter.put("Barrel", new HashMap<>());
        converter.get("Barrel").put("Barrel", 1.0);
        converter.get("Barrel").put("L", 158.987);
        converter.get("Barrel").put("M^3", 0.159);
        converter.get("Barrel").put("Gallon", 42.003);
        converter.get("Barrel").put("Pint", 336.024);
        converter.get("Barrel").put("Quart", 168.012);
        converter.get("Barrel").put("Cubic foot", 5.615);
        converter.get("Barrel").put("Cubic inch", 9702.72);

        converter.put("Cubic foot", new HashMap<>());
        converter.get("Cubic foot").put("Cubic foot", 1.0);
        converter.get("Cubic foot").put("L", 28.317);
        converter.get("Cubic foot").put("M^3", 0.028);
        converter.get("Cubic foot").put("Gallon", 7.481);
        converter.get("Cubic foot").put("Pint", 59.848);
        converter.get("Cubic foot").put("Quart", 29.924);
        converter.get("Cubic foot").put("Barrel", 0.178);
        converter.get("Cubic foot").put("Cubic inch", 1728.0);

        converter.put("Cubic inch", new HashMap<>());
        converter.get("Cubic inch").put("Cubic inch", 1.0);
        converter.get("Cubic inch").put("L", 0.016);
        converter.get("Cubic inch").put("M^3", 0.00002);
        converter.get("Cubic inch").put("Gallon", 0.004);
        converter.get("Cubic inch").put("Pint", 0.035);
        converter.get("Cubic inch").put("Quart", 0.017);
        converter.get("Cubic inch").put("Barrel", 0.0001);
        converter.get("Cubic inch").put("Cubic foot", 0.001);
    }

    class SubmitListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Double numToConvert = Double.parseDouble(frField.getText());
            Double conversionRate = converter.get(fromBox.getSelectedItem()).get(toBox.getSelectedItem());
            String result = new DecimalFormat("#0.000000000").format(numToConvert * conversionRate);
            toField.setText(result);

        }
    }
}
