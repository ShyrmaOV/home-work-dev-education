package com.github.converter;

public class ApplicationConverter {
    public static void main(String args[])
    { // Creating object of demo class.
        ChooseConverter f = new ChooseConverter();

        // Setting Bounds of JFrame.
        f.setBounds(100, 100, 300, 300);

        // Setting Title of frame.
        f.setTitle("RadioButtons");

        // Setting Visible status of frame as true.
        f.setVisible(true);
    }
}
