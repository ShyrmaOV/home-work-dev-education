package com.github.drawingStarsFigures;

public class Main {
    public static void main(String[] args) {
        Figures figures = new Figures();
        figures.firstMatrix();
        figures.secondMatrix();
        figures.thirdMatrix();
        figures.fourthMatrix();
        figures.fifthMatrix();
        figures.sixthMatrix();
        figures.seventhMatrix();
        figures.eighthMatrix();
        figures.ninthMatrix();
    }
}
