package com.github.drawingStarsFigures;

public class Figures {
    public void firstMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++){
                System.out.print("*\t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void secondMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
                for (int j = 0; j < matrix[1].length; j++) {
                    if (i == 0 || j == 0 || i == 6 || j == 6)
                        System.out.print("*\t");
                    else
                        System.out.print("\t");
                }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void thirdMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i == 0 || j == 0 || i == (matrix[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void fourthMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i == 0 || j == 6 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void fifthMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i == 6 || j == 6 || i == (matrix[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void sixthMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i == 6 || j == 0 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void seventhMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i == (matrix[1].length - j - 1) || i == j)
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void eighthMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {
                if (i > matrix[0].length/2)
                    break;
                if (i == 0 || i == (matrix[1].length - j - 1) || i == j)
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
    public void ninthMatrix() {
        String[][] matrix = new String[7][7];
        for (int i = 0; i < matrix[0].length; i++){
            for (int j = 0; j < matrix[1].length; j++) {

                if (i >= matrix[0].length/2 && (i == 6 || i == (matrix[1].length - j - 1) || i == j))
                    System.out.print("*\t");
                else
                    System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.println("-------------------------");
    }
}
