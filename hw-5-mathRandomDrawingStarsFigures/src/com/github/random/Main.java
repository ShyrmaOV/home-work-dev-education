package com.github.random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        // 1. Вывести на консоль случайное число.
        random.randomNumber();
        // 2. Вывести на консоль 10 случайных чисел.
        random.nRandomNumber(10);
        // 3. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10.
        random.nRandomNumberWithRange(10, 0, 10);
        // 4. Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50.
        random.nRandomNumberWithRange(10, 20, 50);
        // 5. Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10.
        random.nRandomNumberWithRange(10, -10, 10);
        // 6. Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.
        random.randomNRandomNumberWithRange(3, 15, -10, 35);

    }
}
