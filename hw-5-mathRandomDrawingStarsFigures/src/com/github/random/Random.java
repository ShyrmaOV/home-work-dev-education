package com.github.random;

public class Random {
    public void randomNumber() {
        System.out.println(Math.random());
        System.out.println("--------------------------");
    }
    public void nRandomNumber(int n) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = (int)(Math.random()*10);
            System.out.print("a[" + i + "] = " + array[i] + "; ");
        }
        System.out.println("\n--------------------------");
    }
    public void nRandomNumberWithRange(int n, int min, int max) {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = min + (int)(Math.random() * (max - min + 1));
            System.out.print("a[" + i + "] = " + array[i] + "; ");
        }
        System.out.println("\n--------------------------");
    }
    public void randomNRandomNumberWithRange(int minN, int maxN, int min, int max) {
        int n = minN + (int)(Math.random() * (maxN - minN + 1));
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = min + (int)(Math.random() * (max - min + 1));
            System.out.print("a[" + i + "] = " + array[i] + "; ");
        }
        System.out.println("\n--------------------------");
    }
}
