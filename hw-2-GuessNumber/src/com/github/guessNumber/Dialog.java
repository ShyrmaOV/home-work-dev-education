package com.github.guessNumber;

public class Dialog {

    private int number = 0;
    private int counter = 0;
    private boolean isRun = true;

    public void run() {
        CreateHideNumber hideNumber = new CreateHideNumber();
        System.out.println("Введите диапазон чисел минимум = 1, максимум = 200:");
        hideNumber.setRange(ScannerWrapper.getNumber(), ScannerWrapper.getNumber());
        hideNumber.createArray();
        System.out.println("Привет, я загадал число от " + hideNumber.getMin() + " до " + hideNumber.getMax() + " вашего диапазона. Попробуй угадать его за 5 попыток!");
        while (isRun) {

            if (counter >= 5) {
                System.out.println("Ты проиграл(");
                isRun = false;
                break;
            }
            System.out.print("Твое число: ");
            number = ScannerWrapper.getNumber();
            if (number > hideNumber.getHideNumber()) {
                System.out.println("Твое число слишком большое. Попробуй меньше");
            } else if (number < hideNumber.getHideNumber()) {
                System.out.println("Твое число слишком маленькое. Попробуй больше");
            } else {
                System.out.println("Поздравляю! Ты угадал задуманное число за " + (counter + 1) + " попыток");
                isRun = false;
            }
            counter++;
        }
    }
}
