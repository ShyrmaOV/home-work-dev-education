package com.github.guessNumber;

public class CreateHideNumber {
    private int hideNumber;
    private int min;
    private int max;
    private int range;
    private int temp;

    public void setRange(int min, int max){
        this.min = min;
        this.max = max;

    }

    public int getMin(){
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getRange(){
        range = max - min;
        return range;
    }
    public int getHideNumber() {
        hideNumber = min + (int) (Math.random() * max);
        return hideNumber;
    }

    public void createArray(){
        temp = min;
        int[] array = new int[getRange() + 1];
        for (int i = 0; i < array.length; i++) {
            array[i] = temp;
            temp++;
        }
    }
}
