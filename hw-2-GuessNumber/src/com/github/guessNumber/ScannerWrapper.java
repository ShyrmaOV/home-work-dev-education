package com.github.guessNumber;

import java.util.Scanner;

public class ScannerWrapper {

    static private Scanner SCANNER = new Scanner(System.in);

    static int getNumber() {
        return SCANNER.nextInt();
    }
}
