package org.bitbucket.nioServer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class SimpleHttpResponse {
    private String protocol;

    private int statusCode;

    private String statusText;

    private Map<String, String> headers = new HashMap<>();

    private String body;

    public SimpleHttpResponse() {
    }

    public SimpleHttpResponse(String protocol, int statusCode, String statusText, Map<String, String> headers, String body) {
        this.protocol = protocol;
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.headers = headers;
        this.body = body;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void addHeader(String header, String value) {
        this.headers.put(header, value);
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.protocol).append(" ").append(this.statusCode).append(" ").append(this.statusText).append("\n\r");
        for (String header : this.headers.keySet()) {
            builder.append(header).append(": ").append(this.headers.get(header)).append("\n\r");
        }
        builder.append("\n\r");
        builder.append(this.body).append("\n\r\n\r");
        return builder.toString();
    }

    public void sendResponse(Socket socket) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        writer.write(this.toString());
        writer.flush();
        writer.close();
    }
}
