package org.bitbucket.nioServer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SimpleHttpRequest {

    private String method;

    private String body;

    private final Map<String, String> headers;

    private char[] protocol;

    public String getBody() {
        return this.headers.get("Body:");
    }

    public SimpleHttpRequest(SocketChannel socket) throws IOException {
        this.headers = parseRequest(takeRequest(socket));
    }

    public String takeRequest(SocketChannel socket) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        String req = "";
        int count = 0;
        while ((count = socket.read(buffer)) > -1) {
            byte[] bytes = buffer.array();
            req = req + new String(bytes);
            System.out.println("request: " + req);
            buffer.clear();
            if(count < 1024){
                break;
            }
        }
        return req;
    }

    public static Map<String, String> parseRequest(String request) throws IOException {
        String[] map = new String[2];
        Map<String, String> header = new HashMap<>();
        List<String> splitRequest = Stream.of(request.split("\n"))
                .map (String::new)
                .collect(Collectors.toList());
        int count = 0;
        for(String str:splitRequest) {
            count++;
            if (count == 1) {
                map = str.split(" ", 2);
                header.put("TypeRequest:", map[0]);
            } else {
                if(str != null && !str.isEmpty() && !str.equals("\r")){
                    map = str.split(":", 2);
                    header.put(map[0], map[1]);

                }
                if (map[0].equals("Content-Length") && Objects.equals(str, "\r")) {
                    int index = Integer.parseInt(map[1].trim());
                    String body = splitRequest.get(splitRequest.size()-1).substring(0,index);
                    header.put("Body:", body);
                }
                if(Objects.equals(str, "\r")){
                    break;
                }
            }
        }
        System.out.println(request);
        return header;
    }

    public String getMethod() {
        return method;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.method).append(" / ").append(this.protocol).append("\n\r");
        for (String header : this.headers.keySet()) {
            builder.append(header).append(": ").append(this.headers.get(header)).append("\n\r");
        }
        builder.append("\n\r");
        builder.append(this.body).append("\n\r\n\r");
        return builder.toString();
    }
}
