package com.github.conditionalStatements;

import com.github.ScannerWrapper;

public class CoordinateQuarter {
    public static int quarter = 1;
    public static void main(String[] args) {

        System.out.println("Coordinate quarter is " + coordinateQuarter(ScannerWrapper.getInt(), ScannerWrapper.getInt()));
    }
    public static int coordinateQuarter(int x, int y) {
        if (x > 0 && y > 0)
            quarter = 1;
        else if (x > 0 && y < 0)
            quarter = 2;
        else if (x < 0 && y < 0)
            quarter = 3;
        else if (x < 0 && y > 0)
            quarter = 4;
        return quarter;
    }
}
