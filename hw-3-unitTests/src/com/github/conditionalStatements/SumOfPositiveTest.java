package com.github.conditionalStatements;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumOfPositiveTest {

    @Test
    public void sumOfPositive() {
        int exp = 4;
        int act = SumOfPositive.sumOfPositive(1, 3, 0);
        Assert.assertEquals(exp, act);
    }
}