package com.github.conditionalStatements;

import com.github.ScannerWrapper;

public class SumOfPositive {
    public static int sum = 0;
    public static void main(String[] args) {
        System.out.println ("Sum is " + sumOfPositive(ScannerWrapper.getInt(), ScannerWrapper.getInt(), ScannerWrapper.getInt()));
    }
    public static int sumOfPositive(int a, int b, int c){
        if (a >= 0)
            sum += a;
        if (b >= 0)
            sum += b;
        if (c >= 0)
            sum += c;
        return sum;
    }
}
