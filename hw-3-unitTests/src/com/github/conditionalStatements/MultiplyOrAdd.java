package com.github.conditionalStatements;

import com.github.ScannerWrapper;

public class MultiplyOrAdd {

    public static int result = 0;
    public static void main(String[] args) {
        System.out.println("Result is " + multiplyOrAdd(ScannerWrapper.getInt(), ScannerWrapper.getInt()));
    }
    public static int multiplyOrAdd(int a, int b){
        if(a % 2 == 0)
            return result = a * b;
        else
            return result = a + b;

    }
}
