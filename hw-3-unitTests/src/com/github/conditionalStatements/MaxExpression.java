package com.github.conditionalStatements;

import com.github.ScannerWrapper;

public class MaxExpression {
    public static int result;
    public static void main(String[] args) {
        System.out.println(maxExpression(ScannerWrapper.getInt(), ScannerWrapper.getInt(), ScannerWrapper.getInt()));
    }
    public static int maxExpression(int a, int b, int c) {
        int arg1 = a * b * c;
        int arg2 = a + b + c;
        result = (Math.max( arg1, arg2)) + 3;
        return result;
    }
}
