package com.github.conditionalStatements;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaxExpressionTest {

    @Test
    public void maxExpressionMultiply() {
        int exp = 11;
        int act = MaxExpression.maxExpression(2, 2, 2);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void maxExpressionAdd() {
        int exp = 7;
        int act = MaxExpression.maxExpression(1, 1, 2);
        Assert.assertEquals(exp, act);
    }
}