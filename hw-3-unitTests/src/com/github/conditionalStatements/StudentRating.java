package com.github.conditionalStatements;

import com.github.ScannerWrapper;

public class StudentRating {
    public static String score = "";
    public static void main(String[] args) {

        System.out.println("Score " + studentRating(ScannerWrapper.getInt()) );
    }
    public static String studentRating(int rating) {
        if (rating >= 0 && rating < 20)
            score = "F";
        if (rating >= 20 && rating < 40)
            score = "E";
        if (rating >= 40 && rating < 60)
            score = "D";
        if (rating >= 60 && rating < 75)
            score = "C";
        if (rating >= 75 && rating < 90)
            score = "B";
        if (rating >= 90 && rating <= 100)
            score = "A";
        return score;
    }
}
