package com.github.conditionalStatements;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StudentRatingTest {

    @Test
    public void studentRatingF() {
        String exp = "F";
        String act = StudentRating.studentRating(10);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void studentRatingE() {
        String exp = "E";
        String act = StudentRating.studentRating(30);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void studentRatingD() {
        String exp = "D";
        String act = StudentRating.studentRating(50);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void studentRatingC() {
        String exp = "C";
        String act = StudentRating.studentRating(70);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void studentRatingB() {
        String exp = "B";
        String act = StudentRating.studentRating(80);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void studentRatingA() {
        String exp = "A";
        String act = StudentRating.studentRating(92);
        Assert.assertEquals(exp, act);
    }
}