package com.github.conditionalStatements;

import org.junit.Assert;
import org.junit.Test;

public class MultiplyOrAddTest {

    @Test
    public void multiplyOrAddOdd() {
        int exp = 12;
        int act = MultiplyOrAdd.multiplyOrAdd(4, 3);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void multiplyOrAddEven() {
        int exp = 6;
        int act = MultiplyOrAdd.multiplyOrAdd(3, 3);
        Assert.assertEquals(exp, act);
    }
}
