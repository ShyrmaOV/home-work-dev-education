package com.github.conditionalStatements;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CoordinateQuarterTest {

    @Test
    public void coordinateQuarterFirst() {
        int exp = 1;
        int act = CoordinateQuarter.coordinateQuarter(1, 1);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void coordinateQuarterSecond() {
        int exp = 2;
        int act = CoordinateQuarter.coordinateQuarter(1, -1);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void coordinateQuarterThird() {
        int exp = 3;
        int act = CoordinateQuarter.coordinateQuarter(-1, -1);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void coordinateQuarterFour() {
        int exp = 4;
        int act = CoordinateQuarter.coordinateQuarter(-1, 1);
        Assert.assertEquals(exp, act);
    }
}