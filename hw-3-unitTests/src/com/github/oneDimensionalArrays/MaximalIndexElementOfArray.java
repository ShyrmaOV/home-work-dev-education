package com.github.oneDimensionalArrays;

public class MaximalIndexElementOfArray {
    public static int[] array = new int[] {3, 6, 4, 5, 7};
    public static void main(String[] args) {
        System.out.println("Maximal index element of massive is " + maximalIndexElement());
    }
    public static int maximalIndexElement() {
        int max = array[0];
        int maxIndex = 0;
        for (int i = 1; i < array.length; i++)
            if (max < array[i]){
                max = array[i];
                maxIndex = i;
            }

        return maxIndex;
    }
}
