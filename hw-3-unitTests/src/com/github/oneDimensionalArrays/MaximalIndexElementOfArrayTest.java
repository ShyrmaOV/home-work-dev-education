package com.github.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaximalIndexElementOfArrayTest {

    @Test
    public void maximalIndexElement() {
        int exp = 4;
        int act = MaximalIndexElementOfArray.maximalIndexElement();
        Assert.assertEquals(exp, act);
    }
}