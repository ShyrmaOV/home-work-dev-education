package com.github.oneDimensionalArrays;

public class ReverseArray {
    public static int[] array = new int[] {3, 1, 4, 5, 7};
    public static int[] reverseArray = new int[array.length];
    public static void main(String[] args) {
        reverseArray();
        for (int i = 0; i < array.length; i++)
            System.out.println(getReverseArray(i));


    }
    public static void reverseArray() {

        int j = 0;
        System.out.println("Revers massive:");
        for (int i = array.length - 1; i >= 0; i--) {
            reverseArray[j] = array[i];
            j++;
        }

    }
    public static int getReverseArray(int i) {
        return reverseArray[i];
    }
}
