package com.github.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumElemEvenIndexTest {

    @Test
    public void sumElemEvenIndex() {
        int exp = 6;
        int act = SumElemEvenIndex.sumElemEvenIndex();
        Assert.assertEquals(exp, act);
    }
}