package com.github.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MinimalElementOfArrayTest {

    @Test
    public void minimalElementOfArray() {
        int exp = 2;
        int act = MinimalElementOfArray.minimalElement();
        Assert.assertEquals(exp, act);
    }
}