package com.github.oneDimensionalArrays;

public class MinimalElementOfArray {
    public static int[] array = new int[] {3, 6, 2, 5, 7};
    public static void main(String[] args) {
        System.out.println("Minimal element of massive is " + minimalElement());
    }
    public static int minimalElement() {
        int min = array[0];
        for (int i = 1; i < array.length; i++)
            if (min > array[i])
                min = array[i];
            return min;
    }
}
