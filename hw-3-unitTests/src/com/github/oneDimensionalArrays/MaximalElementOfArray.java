package com.github.oneDimensionalArrays;

public class MaximalElementOfArray {
    public static int[] array = new int[] {3, 6, 2, 5, 7};
    public static void main(String[] args) {
        System.out.println("maximal element of massive is " + maximalElement());
    }
    public static int maximalElement() {
        int max = array[0];
        for (int i = 1; i < array.length; i++)
            if (max < array[i])
                max = array[i];
        return max;
    }
}
