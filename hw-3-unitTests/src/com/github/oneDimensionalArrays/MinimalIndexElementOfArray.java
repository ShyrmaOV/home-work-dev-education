package com.github.oneDimensionalArrays;

public class MinimalIndexElementOfArray {
    public static int[] array = new int[] {3, 6, 4, 5, 7};
    public static void main(String[] args) {
        System.out.println("Minimal index element of massive is " + minimalIndexElement());
    }
    public static int minimalIndexElement() {
        int min = array[0];
        int minIndex = 0;
        for (int i = 1; i < array.length; i++)
            if (min > array[i]){
                min = array[i];
                minIndex = i;
            }

        return minIndex;
    }
}
