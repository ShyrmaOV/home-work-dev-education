package com.github.oneDimensionalArrays;

public class SumElemEvenIndex {
    public static int[] array = new int[] {3, 1, 4, 5, 7};
    public static void main(String[] args) {
        System.out.println("Sum of element is " + sumElemEvenIndex());
    }
    public static int sumElemEvenIndex() {
        int sumElemOddIndex = 0;
        for (int i = 1; i < array.length; i++)
            if(i % 2 == 1) {
                sumElemOddIndex += array[i];
            }
        return sumElemOddIndex;
    }
}
