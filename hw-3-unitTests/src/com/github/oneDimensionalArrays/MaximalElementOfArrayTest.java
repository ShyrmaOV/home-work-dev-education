package com.github.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MaximalElementOfArrayTest {

    @Test
    public void maximalElement() {
        int exp = 7;
        int act = MaximalElementOfArray.maximalElement();
        Assert.assertEquals(exp, act);
    }
}