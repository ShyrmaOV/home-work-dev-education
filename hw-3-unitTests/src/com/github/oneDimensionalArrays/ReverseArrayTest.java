package com.github.oneDimensionalArrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReverseArrayTest {

    @Test
    public void getReverseArray() {
        int[] exp = new int[] {3, 1, 4, 5, 7};
        int[] act = new int[ReverseArray.array.length];
        for (int i = 0; i < ReverseArray.array.length; i++) {
            act[i] = ReverseArray.getReverseArray(i);
        }
        Assert.assertArrayEquals(exp, act);
    }
}